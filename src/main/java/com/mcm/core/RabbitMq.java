package com.mcm.core;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.bukkit.Bukkit;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class RabbitMq {

    public static ConnectionFactory factory;

    public static void connect() {
        factory = new ConnectionFactory();
        factory.setHost("127.0.0.1");
        factory.setPort(5672);
    }

    public static void send(String channel_name, String message) {
        Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, () -> {
            try (Connection connection = RabbitMq.factory.newConnection()) {
                Channel channel = connection.createChannel();
                channel.queueDeclare(channel_name, true, false, true, null);
                channel.basicPublish("", channel_name, null, message.getBytes());
            } catch (TimeoutException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
