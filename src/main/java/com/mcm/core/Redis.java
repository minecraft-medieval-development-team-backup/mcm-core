package com.mcm.core;

import redis.clients.jedis.Jedis;

public class Redis {

    public static Jedis localhost;

    public static void connect() {
        localhost = new Jedis("127.0.0.1", 6379);
        localhost.auth("VXHYjwcdERTmT8m2iJn3lYnA5WAGF+jeDdNK/RGYiHqxNnJohucJJ/Hw36Ke60YQhNV/AFBP5a+kCHp0");
        localhost.connect();
    }

    public static void disconnect() {
        localhost.close();
    }
}
