package com.mcm.core;

import com.mcm.core.cache.TpaExpired;
import com.mcm.core.database.*;
import com.mcm.core.listeners.PlayerJoinEvents;
import com.mcm.core.serverswitch.PlayerQuitEvents;
import org.bukkit.Bukkit;

public class RegisterMain {

    public static void register() {
        Main.plugin.getServer().getMessenger().registerOutgoingPluginChannel(Main.plugin, "BungeeCord");
        Main.plugin.getServer().getMessenger().registerIncomingPluginChannel(Main.plugin, "BungeeCord", new PluginMessage());
        StoreDb.setStoreInCacheGUI();
        StoreDb.setStoreAdminOnCache();
        StoreDb.setStoreOnCache();
        MercadoDb.setMercadoOnCache();
        Bukkit.getPluginManager().registerEvents(new PlayerJoinEvents(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new com.mcm.core.serverswitch.PlayerJoinEvents(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new PlayerQuitEvents(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new com.mcm.core.listeners.PlayerQuitEvents(), Main.plugin);
        if (Main.server_name == null) Main.server_name = ServerStatusDb.getServerName(Main.plugin.getServer().getIp() + ":" + Main.plugin.getServer().getPort());
        RMqChatManager.syncChat();
        ServerStatusDb.updateStatus(Main.plugin.getServer().getIp() + ":" + Main.plugin.getServer().getPort(), true);
        TpaExpired.taskTpaExpired();
        MinesDb.setMinesOnCache();
    }
}
