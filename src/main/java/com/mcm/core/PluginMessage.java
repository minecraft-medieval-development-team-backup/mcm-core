package com.mcm.core;

import com.google.common.collect.Iterables;
import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.mcm.core.cache.FixProblem1;
import com.mcm.core.cache.Last24h;
import com.mcm.core.cache.PlayerLocation;
import com.mcm.core.cache.StoreSignAdmin;
import com.mcm.core.database.StoreDb;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Sign;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.messaging.PluginMessageListener;
import org.bukkit.util.EulerAngle;

public class PluginMessage implements PluginMessageListener {

    @Override
    public void onPluginMessageReceived(String channel, Player player, byte[] message) {
        if (!channel.equals("BungeeCord")) {
            return;
        }
        //input = send, output = receive
        ByteArrayDataInput in = ByteStreams.newDataInput(message);
        String subchannel = in.readUTF();

        if (subchannel.equals("update_last24h")) {
            Last24h.last24h = in.readInt();
        }

        if (subchannel.equals("player_location")) {
            String received = in.readUTF();
            String[] split = received.split(":");
            String uuidOffline = Bukkit.getOfflinePlayer(split[0]).getUniqueId().toString();
            if (PlayerLocation.get(uuidOffline) == null) new PlayerLocation(uuidOffline, split[1]);
        }

        if (subchannel.equals("GetServer")) {
            Main.server_name = in.readUTF();
        }

        // && Bukkit.getWorld("spawn") != null
        if (subchannel.equals("shop_admin_create")) {
            // world:x:y:z  /  material-durability:quantity:purchase:sale  /  signdirection:sign.getLine(0):sign.getLine(1):sign.getLine(2):sign.getLine(3)  /  world:x:y:z:yaw:pitch:id:customnamevisible:headpose-x:headpose-y:headpose-z:small:visible:gravity:material:durability
            String received = in.readUTF();
            if (StoreSignAdmin.get(received.split("/")[0]) != null) {
                String[] splitSA = received.split("/")[1].split(":");
                new StoreSignAdmin(received.split("/")[0], splitSA[0], Integer.valueOf(splitSA[1]), Double.parseDouble(splitSA[2]), Double.parseDouble(splitSA[3])).insert();

                String[] splitLo = received.split("/")[0].split(":");
                Location location = new Location(Bukkit.getWorld(splitLo[0]), Integer.valueOf(splitLo[1]), Integer.valueOf(splitLo[2]), Integer.valueOf(splitLo[3]));
                location.getBlock().setType(Material.OAK_WALL_SIGN);
                Sign sign = (Sign) location.getBlock().getState();

                String[] splitSign = received.split("/")[2].split(":");
                org.bukkit.material.Sign matSign = new org.bukkit.material.Sign(Material.OAK_WALL_SIGN);
                matSign.setFacingDirection(BlockFace.valueOf(splitSign[0]));
                sign.setData(matSign);
                sign.setLine(0, splitSign[1] + ": " + splitSign[2]);
                sign.setLine(1, splitSign[3]);
                sign.setLine(2, splitSign[4]);
                sign.setLine(3, splitSign[5]);
                sign.update();

                String[] splitAs = received.split("/")[3].split(":");
                Location loc = new Location(Bukkit.getWorld(splitAs[0]), Double.parseDouble(splitAs[1]), Double.parseDouble(splitAs[2]) + 1, Double.parseDouble(splitAs[3]), Float.parseFloat(splitAs[4]), Float.parseFloat(splitAs[5]));
                ArmorStand armorStand = loc.getWorld().spawn(loc, ArmorStand.class);
                armorStand.setCustomName(splitAs[6]);
                if (splitAs[7].equals("false")) armorStand.setCustomNameVisible(false);
                else armorStand.setCustomNameVisible(true);
                EulerAngle headPose = new EulerAngle(Double.parseDouble(splitAs[8]), Double.parseDouble(splitAs[9]), Double.parseDouble(splitAs[10]));
                armorStand.setHeadPose(headPose);
                if (splitAs[11].equals("false")) armorStand.setSmall(false);
                else armorStand.setSmall(true);
                if (splitAs[12].equals("false")) armorStand.setVisible(false);
                else armorStand.setVisible(true);
                if (splitAs[13].equals("false")) armorStand.setGravity(false);
                else armorStand.setGravity(true);
                ItemStack helmet = new ItemStack(Material.getMaterial(splitAs[14]), Short.parseShort(splitAs[15]));
                armorStand.setHelmet(helmet);
            }
        }

        if (subchannel.equals("shop_admin_remove") && Bukkit.getWorld("spawn") != null) {
            String received = in.readUTF();
            String[] loc = received.split(":");
            Location location = new Location(Bukkit.getWorld(loc[0]), Integer.valueOf(loc[1]), Integer.valueOf(loc[2]), Integer.valueOf(loc[3]));
            location.getBlock().setType(Material.AIR);
            for (Entity entity : location.getWorld().getNearbyEntities(location, 1, 1, 1)) {
                if (entity instanceof ArmorStand) entity.remove();
                break;
            }
            StoreDb.removeStoreAdmin(received);
        }

        if (subchannel.equals("update_fix_problem_economy_1")) {
            String received = in.readUTF();
            if (FixProblem1.get(received) == null) new FixProblem1(received).insert();
        }

        if (subchannel.equals("update_item_cache")) {
            String received = in.readUTF();
            for (String category : com.mcm.core.database.StoreDb.categories()) {
                for (int i = 0; i < com.mcm.core.database.StoreDb.getItensGUI(category).size(); i++) {
                    String[] split = com.mcm.core.database.StoreDb.getItensGUI(category).get(i).split(":");
                    String[] spl = received.split(":");
                    if (split[0].equalsIgnoreCase(spl[0])) {
                        com.mcm.core.database.StoreDb.getItensGUI(category).set(i, received);
                    }
                }
            }
        }
    }

    public static void onPluginMessageSend(String subChannel, String update) {
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF(subChannel);
        if (update != null) out.writeUTF(update);

        Player player = Iterables.getFirst(Bukkit.getOnlinePlayers(), null);
        player.sendPluginMessage(Main.plugin, "BungeeCord", out.toByteArray());
    }
}
