package com.mcm.core.api.object;

import com.mcm.core.api.handler.ThreadHandler;
import java.util.ArrayList;
import java.util.HashMap;

public class Threads {

    public static ArrayList<Integer> ids;
    public static ArrayList<Long> times;

    private int id;
    private long milis;
    private ThreadHandler handler;
    private static HashMap<Integer, Threads> cache = new HashMap<>();

    public Threads(ThreadHandler handler, long milis) {
        if (ids == null) ids = new ArrayList<>();
        if (times == null) times = new ArrayList<>();
        this.id = ids.size() + 1;
        this.milis = milis;
        this.handler = handler;
        ids.add(ids.size() + 1);
        if (!times.contains(milis)) times.add(milis);
    }

    public Threads insert() {
        this.cache.put(this.id, this);
        return this;
    }

    public static Threads get(int id) {
        return cache.get(id);
    }

    public void delete() {
        cache.remove(this.id);
    }

    public long getMilis() {
        return milis;
    }

    public void setMilis(long milis) {
        this.milis = milis;
    }

    public ThreadHandler getHandler() {
        return handler;
    }

    public void setHandler(ThreadHandler handler) {
        this.handler = handler;
    }
}
