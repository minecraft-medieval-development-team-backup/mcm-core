package com.mcm.core.api;

import com.mcm.core.Main;
import com.mcm.core.api.handler.ThreadHandler;
import com.mcm.core.api.object.Threads;
import org.bukkit.Bukkit;

public class ThreadAPI {

    public static int createThread(ThreadHandler handler, long milis) {
        //milis = milis / 60000;
        if (Threads.times != null) {
            if (!Threads.times.contains(milis)) runLater(milis);
        } else runLater(milis);

        new Threads(handler, milis).insert();
        return Threads.ids.size();
    }

    public static boolean deleteThread(int thread) {
        Threads.get(thread).delete();
        if (Threads.get(thread) == null) return true;
        else return false;
    }

    //---

    private static void run(long milis) {
        Thread thread = new Thread(() -> {
            while (true) {
                try {
                    Thread.sleep(milis);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                boolean isUsed = false;
                if (!Threads.ids.isEmpty()) {
                    for (Integer id : Threads.ids) {
                        if (Threads.get(id) != null && Threads.get(id).getMilis() == milis) {
                            isUsed = true;
                            Threads.get(id).getHandler().handle();
                        }
                    }
                }

                if (!isUsed) break;
            }
        });
        thread.start();
        thread.setName(milis + "-thread");
    }

    private static void runLater(long milis) {
        Bukkit.getScheduler().runTaskLaterAsynchronously(Main.plugin, () -> {
            run(milis);
        }, 5L);
    }
}
