package com.mcm.core.api.handler;

@FunctionalInterface
public interface ThreadHandler {

    void handle();
}
