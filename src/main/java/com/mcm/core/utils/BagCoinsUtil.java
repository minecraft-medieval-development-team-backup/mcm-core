package com.mcm.core.utils;

import com.mcm.core.Main;
import net.minecraft.server.v1_15_R1.NBTTagCompound;
import net.minecraft.server.v1_15_R1.NBTTagList;
import net.minecraft.server.v1_15_R1.NBTTagString;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_15_R1.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class BagCoinsUtil {

    public static DecimalFormat formatter = new DecimalFormat("#,###.00");

    public static void addCoins(Player player, int value) {
        boolean added = false;
        for (int i = 0; i <= player.getInventory().getSize(); i++) {
            if (player.getInventory().getItem(i) != null && !player.getInventory().getItem(i).getType().equals(Material.AIR)) {
                if (NBTTagUtil.getIntData(player.getInventory().getItem(i), "coins") != 0) {
                    NBTTagUtil.setData(player.getInventory().getItem(i), "coins", NBTTagUtil.getIntData(player.getInventory().getItem(i), "coins") + value);
                    added = true;

                    ItemMeta meta = player.getInventory().getItem(i).getItemMeta();
                    ArrayList<String> lore = new ArrayList<>();
                    lore.add(" ");
                    lore.add(ChatColor.GRAY + formatter.format(NBTTagUtil.getIntData(player.getInventory().getItem(i), "coins")));
                    meta.setLore(lore);
                    player.getInventory().getItem(i).setItemMeta(meta);
                    break;
                }
            }
        }

        if (!added) {
            HashMap<Integer, ItemStack> items = player.getOpenInventory().getBottomInventory().addItem(getCoinsItem(player, value));
            for (Map.Entry<Integer, ItemStack> entry : items.entrySet()) {
                player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
            }
        }
    }

    public static boolean addCoins(Inventory inventory, int value) {
        boolean added = false;
        for (int i = 0; i <= inventory.getSize(); i++) {
            if (inventory.getItem(i) != null && !inventory.getItem(i).getType().equals(Material.AIR)) {
                if (NBTTagUtil.getIntData(inventory.getItem(i), "coins") != 0) {
                    NBTTagUtil.setData(inventory.getItem(i), "coins", NBTTagUtil.getIntData(inventory.getItem(i), "coins") + value);
                    added = true;

                    ItemMeta meta = inventory.getItem(i).getItemMeta();
                    ArrayList<String> lore = new ArrayList<>();
                    lore.add(" ");
                    lore.add(ChatColor.GRAY + formatter.format(NBTTagUtil.getIntData(inventory.getItem(i), "coins")));
                    meta.setLore(lore);
                    inventory.getItem(i).setItemMeta(meta);
                    break;
                }
            }
        }
        return added;
    }

    public static boolean removeCoins(Player player, int value) {
        for (int i = 0; i <= player.getInventory().getSize(); i++) {
            boolean removed = false;
            if (player.getInventory().getItem(i) != null && !player.getInventory().getItem(i).getType().equals(Material.AIR)) {
                if (NBTTagUtil.getIntData(player.getInventory().getItem(i), "coins") != 0) {
                    if (NBTTagUtil.getIntData(player.getInventory().getItem(i), "coins") >= value) {
                        NBTTagUtil.setData(player.getInventory().getItem(i), "coins", NBTTagUtil.getIntData(player.getInventory().getItem(i), "coins") + value);
                        removed = true;

                        ItemMeta meta = player.getInventory().getItem(i).getItemMeta();
                        ArrayList<String> lore = new ArrayList<>();
                        lore.add(" ");
                        lore.add(ChatColor.GRAY + formatter.format(NBTTagUtil.getIntData(player.getInventory().getItem(i), "coins")));
                        meta.setLore(lore);
                        player.getInventory().getItem(i).setItemMeta(meta);
                        break;
                    } else return false;
                }
            }
            if (removed) return true;
        }
        return false;
    }

    public static boolean haveSpaceOrBagCoins(Player player) {
        boolean have = false;
        for (int i = 0; i <= player.getInventory().getSize(); i++) {
            if (player.getInventory().getItem(i) == null) have = true; else if (player.getInventory().getItem(i) != null && player.getInventory().getItem(i).getType().equals(Material.AIR)) have = true;
            if (NBTTagUtil.getIntData(player.getInventory().getItem(i), "coins") != 0) have = true;
        }
        return have;
    }

    private static ItemStack getCoinsItem(Player player, int value) {
        String id = "da608428-e220-435c-9669-2fe3c0575458";
        String skinValue = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZTM2ZTk0ZjZjMzRhMzU0NjVmY2U0YTkwZjJlMjU5NzYzODllYjk3MDlhMTIyNzM1NzRmZjcwZmQ0ZGFhNjg1MiJ9fX0=";
        ItemStack coins = setSkullOwner(new ItemStack(Material.LEGACY_SKULL_ITEM, 1, (short) 3), id, skinValue);
        SkullMeta metaCoins = (SkullMeta) coins.getItemMeta();
        metaCoins.setDisplayName(Main.getTradution("m89U?y@CaJyhurF", player.getUniqueId().toString()));
        ArrayList<String> loreCoins = new ArrayList<>();
        loreCoins.add(" ");
        loreCoins.add(ChatColor.GRAY + formatter.format(value));
        coins.setItemMeta(metaCoins);
        return coins;
    }

    private static ItemStack setSkullOwner(final ItemStack itemStack, final String id, final String textureValue) {
        final net.minecraft.server.v1_15_R1.ItemStack nmsStack = CraftItemStack.asNMSCopy(itemStack);

        NBTTagCompound compound = nmsStack.getTag();
        if (compound == null) {
            compound = new NBTTagCompound();
            nmsStack.setTag(compound);
            compound = nmsStack.getTag();
        }

        final NBTTagCompound skullOwner = new NBTTagCompound();
        skullOwner.set("Id", NBTTagString.a(textureValue));
        final NBTTagCompound properties = new NBTTagCompound();
        final NBTTagList textures = new NBTTagList();
        final NBTTagCompound value = new NBTTagCompound();
        value.set("Value", NBTTagString.a(textureValue));
        textures.add(value);
        properties.set("textures", textures);
        skullOwner.set("Properties", properties);

        compound.set("SkullOwner", skullOwner);
        nmsStack.setTag(compound);

        return CraftItemStack.asBukkitCopy(nmsStack);
    }
}
