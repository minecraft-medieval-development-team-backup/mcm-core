package com.mcm.core.utils;

import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class InventoryHasSpace {

    public static boolean hasSpace(Inventory inventory, ItemStack item, int amount) {
        int max = item.getMaxStackSize();
        int slotsEmpty = 0;
        int spaceFree = 0;
        for (int i = 0; i < inventory.getSize(); i++) {
            if (inventory.getItem(i) == null || inventory.getItem(i).getType().equals(Material.AIR)) slotsEmpty++;
            if (inventory.getItem(i) != null && inventory.getItem(i).getType().equals(item.getType())) spaceFree += max - inventory.getItem(i).getAmount();
        }

        int result = spaceFree + (max * slotsEmpty);
        if (result >= amount) return true; else return false;
    }

    public static int hasSpace(Inventory inventory, ItemStack item) {
        int max = item.getMaxStackSize();
        int slotsEmpty = 0;
        int spaceFree = 0;
        for (ItemStack i : inventory.getContents()) {
            if (i == null || i.getType().equals(Material.AIR)) slotsEmpty++;
            if (i != null && i.getType().equals(item.getType())) spaceFree += max - i.getAmount();
        }

        return (spaceFree + (max * slotsEmpty)) - 320;
    }
}
