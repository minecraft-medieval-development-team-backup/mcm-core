package com.mcm.core.utils;

import com.mojang.brigadier.exceptions.CommandSyntaxException;
import net.minecraft.server.v1_15_R1.MojangsonParser;
import net.minecraft.server.v1_15_R1.NBTTagCompound;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_15_R1.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;

public class ItemStackSerializer {

    public static ItemStack deserialize(String json) {
        if (json == null || json.equals("empty")) {
            return new ItemStack(Material.AIR);
        }
        try {
            NBTTagCompound comp = MojangsonParser.parse(json);
            net.minecraft.server.v1_15_R1.ItemStack cis = net.minecraft.server.v1_15_R1.ItemStack.a(comp);
            return CraftItemStack.asBukkitCopy(cis);
        } catch (CommandSyntaxException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static String serialize(ItemStack is) {
        if (is == null)
            return "empty";
        net.minecraft.server.v1_15_R1.ItemStack nms = CraftItemStack.asNMSCopy(is);
        NBTTagCompound tag = new NBTTagCompound();
        nms.save(tag);
        return tag.toString();
    }

    /*
    public static ItemStack deserializeItemStack(String data) {
        return stringToItemStack(data);
    }

    public static String serializeItemStack(ItemStack item) {
        return itemToString(item);
    }

    public static ItemStack stringToItemStack(String item) {
        ItemStack is = null;

        String[] serializedItemStack = item.split(":");
        for (String itemInfo : serializedItemStack) {
            String[] itemAttribute = itemInfo.split("@");

            if (itemAttribute[0].equals("t")) {
                is = new ItemStack(Material.getMaterial(itemAttribute[1]));
            } else if (itemAttribute[0].equals("d") ) {
                is.setDurability(Short.valueOf(itemAttribute[1]));
            } else if (itemAttribute[0].equals("a")) {
                is.setAmount(Integer.valueOf(itemAttribute[1]));
            } else if (itemAttribute[0].equals("e")) {
                is.addEnchantment(Enchantment.getByName(itemAttribute[1]), Integer.valueOf(itemAttribute[2]));
            } else if (itemAttribute[0].equals("f")) {
                ItemMeta meta = is.getItemMeta();
                meta.addItemFlags(ItemFlag.valueOf(itemAttribute[1]));
                is.setItemMeta(meta);
            } else if (itemAttribute[0].equals("n")) {
                ItemMeta meta = is.getItemMeta();
                meta.setDisplayName(itemAttribute[1]);
                is.setItemMeta(meta);
            }
        }

        return is;
    }

    public static String itemToString(ItemStack item) {
        String serializedItemStack = "";

        String isType = item.getType().name();
        serializedItemStack += "t@" + isType;

        if (item.getDurability() != 0) {
            String isDurability = String.valueOf(item.getDurability());
            serializedItemStack += ":d@" + isDurability;
        }

        if (item.getAmount() != 1) {
            String isAmount = String.valueOf(item.getAmount());
            serializedItemStack += ":a@" + isAmount;
        }

        Map<Enchantment, Integer> isEnch = item.getEnchantments();
        if (isEnch.size() > 0) {
            for (Map.Entry<Enchantment, Integer> ench : isEnch.entrySet()) {
                serializedItemStack += ":e@" + ench.getKey().getName() + "@" + ench.getValue();
            }
        }

        if (!item.getItemMeta().getItemFlags().isEmpty()) {
            for (Object f : item.getItemMeta().getItemFlags().toArray()) {
                ItemFlag flag = (ItemFlag) f;
                serializedItemStack += ":f@" + flag.name();
            }
        }

        if (item.getItemMeta().hasDisplayName()) {
            serializedItemStack += ":n@" + item.getItemMeta().getDisplayName();
        }
        return serializedItemStack;
    }

     */
}
