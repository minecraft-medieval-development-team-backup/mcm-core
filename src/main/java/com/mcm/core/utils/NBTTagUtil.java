package com.mcm.core.utils;

import com.mcm.core.Main;
import net.minecraft.server.v1_15_R1.NBTTagCompound;
import net.minecraft.server.v1_15_R1.NBTTagString;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_15_R1.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;

public class NBTTagUtil {

    public static ItemStack setData(ItemStack item, String tag, String data) {
        net.minecraft.server.v1_15_R1.ItemStack nmsItem = CraftItemStack.asNMSCopy(item);
        NBTTagCompound compound = (nmsItem.hasTag()) ? nmsItem.getTag() : new NBTTagCompound();
        compound.setString(tag, data);
        nmsItem.setTag(compound);
        return CraftItemStack.asBukkitCopy(nmsItem);
    }

    public static Block setData(Block block, String tag, String data) {
        block.setMetadata(tag, new FixedMetadataValue(Main.plugin, data));
        block.getState().update();
        return block;
    }

    public static ItemStack setData(ItemStack item, String tag, int value) {
        net.minecraft.server.v1_15_R1.ItemStack nmsItem = CraftItemStack.asNMSCopy(item);
        NBTTagCompound compound = (nmsItem.hasTag()) ? nmsItem.getTag() : new NBTTagCompound();
        compound.setInt(tag, value);
        nmsItem.setTag(compound);
        return CraftItemStack.asBukkitCopy(nmsItem);
    }

    public static Block setData(Block block, String tag, int value) {
        block.setMetadata(tag, new FixedMetadataValue(Main.plugin, value));
        block.getState().update();
        return block;
    }

    public static String getStringData(Block block, String tag) {
        if (block.getMetadata(tag) != null && block.getMetadata(tag).size() >= 1 && block.getMetadata(tag).get(0) != null && block.getMetadata(tag).get(0).asString() != null) {
            return block.getMetadata(tag).get(0).asString();
        } else return null;
    }

    public static int getIntData(Block block, String tag) {
        if (block.getMetadata(tag) != null  && block.getMetadata(tag).size() >= 1 && block.getMetadata(tag).get(0) != null && block.getMetadata(tag).get(0).asString() != null) {
            return block.getMetadata(tag).get(0).asInt();
        } else return 0;
    }

    public static String getStringData(ItemStack item, String tag) {
        if (getCompound(item).get(tag) != null && !getCompound(item).get(tag).asString().isEmpty()) return getCompound(item).get(tag).asString(); else return null;
    }

    public static int getIntData(ItemStack item, String tag) {
        return getCompound(item).getInt(tag);
    }

    private static NBTTagCompound getCompound(ItemStack item) {
        net.minecraft.server.v1_15_R1.ItemStack nmsItem = CraftItemStack.asNMSCopy(item);
        return (nmsItem.hasTag()) ? nmsItem.getTag() : new NBTTagCompound();
    }
}
