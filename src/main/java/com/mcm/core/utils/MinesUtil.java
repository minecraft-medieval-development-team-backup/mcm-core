package com.mcm.core.utils;

import com.mcm.core.database.MinesDb;
import org.bukkit.Location;

public class MinesUtil {

    public static int isOnMine(Location loc) {
        if (MinesDb.getIds() != null) {
            for (int id : MinesDb.getIds()) {
                final String[] pos1 = MinesDb.getPos1(id).split(":");
                final String[] pos2 = MinesDb.getPos2(id).split(":");

                if (pos1[0].equalsIgnoreCase(loc.getWorld().getName())) {
                    final int p1x = Integer.valueOf(pos1[1]);
                    final int p1y = Integer.valueOf(pos1[2]);
                    final int p1z = Integer.valueOf(pos1[3]);
                    final int p2x = Integer.valueOf(pos2[1]);
                    final int p2y = Integer.valueOf(pos2[2]);
                    final int p2z = Integer.valueOf(pos2[3]);

                    final int minX = p1x < p2x ? p1x : p2x;
                    final int minY = p1y < p2y ? p1y : p2y;
                    final int minZ = p1z < p2z ? p1z : p2z;

                    final int maxX = p1x > p2x ? p1x : p2x;
                    final int maxY = p1y > p2y ? p1y : p2y;
                    final int maxZ = p1z > p2z ? p1z : p2z;

                    if ((loc.getBlockX() >= minX) && (loc.getBlockX() <= maxX) && (loc.getBlockY() >= minY)
                            && (loc.getBlockY() <= maxY) && (loc.getBlockZ() >= minZ) && (loc.getBlockZ() <= maxZ)) {
                        return id;
                    }
                }
            }
        }
        return 0;
    }
}
