package com.mcm.core;

import com.mcm.core.database.ServerStatusDb;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

    public static Main instance;
    public static Plugin plugin;

    public static String ip = "localhost";
    public static String table = "mcmedieval";
    public static String user = "admin";
    public static String password = "r4TzoVQU";

    public static String server_name;

    @Override
    public void onEnable() {
        Main.instance = this;
        Main.plugin = this;
        MySql.connect();
        Redis.connect();
        RabbitMq.connect();
        RegisterMain.register();
    }

    @Override
    public void onDisable() {
        ServerStatusDb.updateStatus(Main.plugin.getServer().getIp() + ":" + Main.plugin.getServer().getPort(), false);
        Redis.disconnect();
        MySql.disconnect();
    }

    public static String getTradution(String message_id, String uuid) {
        return com.mcm.translator.database.TradutionDb.getMessage(message_id, uuid);
    }
}
