package com.mcm.core.listeners;

import com.mcm.core.Main;
import com.mcm.core.RabbitMq;
import com.mcm.core.database.RMqChatManager;
import com.mcm.core.database.ServerStatusDb;
import com.mcm.core.enums.ServersList;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerQuitEvents implements Listener {

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        String uuid = player.getUniqueId().toString();

        Bukkit.getScheduler().runTaskLaterAsynchronously(Main.plugin, () -> {
            ServerStatusDb.updateOnline(Main.plugin.getServer().getIp() + ":" + Main.plugin.getServer().getPort(), Bukkit.getOnlinePlayers().size());
            for (ServersList server : ServersList.values()) {
                if (!server.name().replaceAll("_", "-").equals(Main.server_name))
                    RabbitMq.send(server.name().replaceAll("_", "-"), RMqChatManager.key + "/online_update=" + Main.server_name + "=" + Bukkit.getOnlinePlayers().size());
            }
        }, 20L);
    }
}
