package com.mcm.core.database;

import com.mcm.core.Main;
import com.mcm.core.MySql;
import org.bukkit.Bukkit;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class PlotsDb {

    public static ArrayList<String> getLocations(String serverName) {
        ArrayList<String> locations = new ArrayList<>();

        try {
            PreparedStatement statement = MySql.connection.prepareStatement("SELECT * FROM Plots_data WHERE servername = ?");
            statement.setString(1, serverName);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                locations.add(rs.getString("location"));
            }
            return locations;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void addPlotLocation(String location) {
        Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, new Runnable() {
            @Override
            public void run() {
                try {
                    PreparedStatement statement = MySql.connection.prepareStatement("INSERT INTO Plots_data(servername, location) VALUES (?, ?)");
                    statement.setString(1, Main.server_name);
                    statement.setString(2, location);
                    statement.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
