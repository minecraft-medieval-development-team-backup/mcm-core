package com.mcm.core.database;

import com.mcm.core.MySql;
import com.mcm.core.cache.Tag;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class TagDb {

    public static void setTag(String uuid, String tag) {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("INSERT INTO Tag_data(uuid, tag) VALUES (?, ?)");
            statement.setString(1, uuid);
            statement.setString(2, tag);
            statement.executeUpdate();

            new Tag(uuid, tag).insert();
            Player player = Bukkit.getPlayer(UUID.fromString(uuid));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static String getTag(String uuid) {
        if (Tag.get(uuid) != null) {
            return Tag.get(uuid).getTag();
        } else {
            String tag = getTagDb(uuid);
            if (tag == null) {
                setTag(uuid, "membro");
                return "membro";
            } else {
                new Tag(uuid, tag).insert();
                return tag;
            }
        }
    }

    public static String getTagDb(String uuid) {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("SELECT * FROM Tag_data WHERE uuid = ?");
            statement.setString(1, uuid);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                return rs.getString("tag");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void updateTag(String uuid, String tag) {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Tag_data SET tag = ? WHERE uuid = ?");
            statement.setString(2, uuid);
            statement.setString(1, tag);
            statement.executeUpdate();

            if (Tag.get(uuid) != null) {
                Tag.get(uuid).setTag(tag);
            } else new Tag(uuid, tag).insert();
            Player player = Bukkit.getPlayer(UUID.fromString(uuid));
            if (player != null && player.isOnline()) com.mcm.scoreboard.utils.ScoreboardManager.updateGroup(player);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
