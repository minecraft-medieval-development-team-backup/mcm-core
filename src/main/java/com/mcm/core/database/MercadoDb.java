package com.mcm.core.database;

import com.mcm.core.Main;
import com.mcm.core.MySql;
import com.mcm.core.RabbitMq;
import com.mcm.core.cache.ItensMercado;
import com.mcm.core.cache.Mercado;
import com.mcm.core.enums.ServersList;
import com.mcm.core.utils.ItemStackSerializer;
import net.minecraft.server.v1_15_R1.NBTTagCompound;
import net.minecraft.server.v1_15_R1.NBTTagString;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.libs.org.apache.commons.lang3.RandomStringUtils;
import org.bukkit.craftbukkit.v1_15_R1.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.Metadatable;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class MercadoDb {

    public static ArrayList<ItensMercado> getItens(String category) {
        if (Mercado.get(category) != null) return Mercado.get(category).getItens(); else return new ArrayList<>();
    }

    public static void removeItem(String id, boolean dbRemoved) {
        Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, () -> {
            try {
                if (dbRemoved == true) {
                    Mercado.get(ItensMercado.get(id).getCategory()).removeItem(id);
                    return;
                }
                PreparedStatement statement = MySql.connection.prepareStatement("DELETE FROM Mercado_data WHERE uuid = ?");
                statement.setString(1, ItensMercado.get(id).getUuid() + id);
                statement.executeUpdate();

                Mercado.get(ItensMercado.get(id).getCategory()).removeItem(id);
                for (ServersList server : ServersList.values()) {
                    if (!server.name().replaceAll("_", "-").equals(Main.server_name))
                        RabbitMq.send(server.name().replaceAll("_", "-"), RMqChatManager.key + "/remove_item_on_mercado=" + id);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public static void addItem(String uuid, String name, String category, ItemStack item, double value) {
        String random = RandomStringUtils.random(15, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789");

        net.minecraft.server.v1_15_R1.ItemStack nms = CraftItemStack.asNMSCopy(item);
        NBTTagCompound compound = (nms.hasTag()) ? nms.getTag() : new NBTTagCompound();
        compound.set("id", NBTTagString.a(random));
        nms.setTag(compound);

        Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, () -> {
            try {
                String itemSerialized = ItemStackSerializer.serialize(CraftItemStack.asBukkitCopy(nms));
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(new Date());
                calendar.add(Calendar.DAY_OF_MONTH, 1);
                java.sql.Date date = new java.sql.Date(calendar.getTime().getTime());

                PreparedStatement statement = MySql.connection.prepareStatement("INSERT INTO Mercado_data(uuid, name, category, item, value, placed) VALUES (?, ?, ?, ?, ?, ?)");
                statement.setString(1, uuid + random);
                statement.setString(2, name);
                statement.setString(3, category);
                statement.setString(4, itemSerialized);
                statement.setDouble(5, value);
                statement.setLong(6, calendar.getTime().getTime());
                statement.executeUpdate();

                ItensMercado a = new ItensMercado(random, uuid, name, category, CraftItemStack.asBukkitCopy(nms), value, date).insert();
                Mercado.get(category).addItem(a);
                for (ServersList server : ServersList.values()) {
                    if (!server.name().replaceAll("_", "-").equals(Main.server_name))
                        RabbitMq.send(server.name().replaceAll("_", "-"), RMqChatManager.key + "/add_item_on_mercado=" + uuid + random + "=" + name + "=" + category + "=" + itemSerialized + "=" + value + "=" + calendar.getTime().getTime());
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public static void setMercadoOnCache() {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("SELECT * FROM Mercado_data");
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                String id = rs.getString("uuid");
                Date date = new Date();
                date.setTime(rs.getLong("placed"));
                ItensMercado a = new ItensMercado(id.substring(36), id.substring(0, 36), rs.getString("name"), rs.getString("category"), ItemStackSerializer.deserialize(rs.getString("item")), rs.getDouble("value"), date).insert();
                Mercado.get(rs.getString("category")).addItem(a);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
