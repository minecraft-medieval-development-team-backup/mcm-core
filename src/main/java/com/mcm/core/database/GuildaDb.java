package com.mcm.core.database;

import com.mcm.core.Main;
import com.mcm.core.MySql;
import com.mcm.core.RabbitMq;
import com.mcm.core.cache.GuildaData;
import com.mcm.core.cache.GuildaPlayer;
import com.mcm.core.enums.ServersList;
import org.bukkit.Bukkit;

import java.io.*;
import java.sql.*;

public class GuildaDb {

    public static void setOnCache(String name) {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("SELECT * FROM Guilda_data WHERE name = ?");
            statement.setString(1, name);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                String n = rs.getString("name");
                if (n != null)
                    if (GuildaData.get(name) != null && GuildaData.get(name).getMembros() == null)
                        GuildaData.get(name).delete();
                new GuildaData(name, rs.getString("membros"), rs.getInt("plot_level"), rs.getInt("coins_stock_level"), rs.getInt("elixir_stock_level"), rs.getInt("elixir_negro_stock_level"), rs.getInt("coins"), rs.getInt("elixir"), rs.getInt("elixir_negro"), rs.getLong("last_coins_collected"), rs.getLong("last_elixir_collected"), rs.getLong("last_elixir_negro_collected"), rs.getInt("generator_coins_level"), rs.getInt("generator_elixir_level"), rs.getInt("generator_elixir_negro_level"), rs.getInt("xp"), rs.getInt("level"), rs.getInt("wins"), rs.getInt("defeats"), rs.getDate("last_presence"), rs.getDate("last_attack_suffered"), rs.getDate("last_attack_carried"), rs.getString("alocated")).insert();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (GuildaData.get(name) == null)
            new GuildaData(name, null, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, null, null, null, null).insert();
    }

    public static String getAlocated(String name) {
        if (GuildaData.get(name) != null) return GuildaData.get(name).getAlocated();
        else setOnCache(name);
        return GuildaData.get(name).getAlocated();
    }

    public static long getLastCoinsCollected(String name) {
        if (GuildaData.get(name) != null)
            return GuildaData.get(name).getLast_coins_collected();
        else setOnCache(name);
        return GuildaData.get(name).getLast_coins_collected();
    }

    public static long getLastElixirCollected(String name) {
        if (GuildaData.get(name) != null)
            return GuildaData.get(name).getLast_elixir_collected();
        else setOnCache(name);
        return GuildaData.get(name).getLast_elixir_collected();
    }

    public static long getLastElixirNegroCollected(String name) {
        if (GuildaData.get(name) != null)
            return GuildaData.get(name).getLast_elixir_negro_collected();
        else setOnCache(name);
        return GuildaData.get(name).getLast_elixir_negro_collected();
    }

    public static java.util.Date getLastAttackCarried(String name) {
        if (GuildaData.get(name) != null)
            return new java.util.Date(GuildaData.get(name).getLast_attack_carried().getTime());
        else setOnCache(name);
        return new java.util.Date(GuildaData.get(name).getLast_attack_carried().getTime());
    }

    public static java.util.Date getLastAttackSuffered(String name) {
        if (GuildaData.get(name) != null)
            return new java.util.Date(GuildaData.get(name).getLast_attack_suffered().getTime());
        else setOnCache(name);
        return new java.util.Date(GuildaData.get(name).getLast_attack_suffered().getTime());
    }

    public static java.util.Date getLastPresence(String name) {
        if (GuildaData.get(name) != null) return new java.util.Date(GuildaData.get(name).getLast_presence().getTime());
        else setOnCache(name);
        return new java.util.Date(GuildaData.get(name).getLast_presence().getTime());
    }

    public static int getDefeats(String name) {
        if (GuildaData.get(name) != null) return GuildaData.get(name).getDefeats();
        else setOnCache(name);
        return GuildaData.get(name).getDefeats();
    }

    public static int getWins(String name) {
        if (GuildaData.get(name) != null) return GuildaData.get(name).getWins();
        else setOnCache(name);
        return GuildaData.get(name).getWins();
    }

    public static int getLevel(String name) {
        if (GuildaData.get(name) != null) return GuildaData.get(name).getLevel();
        else setOnCache(name);
        return GuildaData.get(name).getLevel();
    }

    public static int getXp(String name) {
        if (GuildaData.get(name) != null) return GuildaData.get(name).getXp();
        else setOnCache(name);
        return GuildaData.get(name).getXp();
    }

    public static int getGeneratorelixir_negroLevel(String name) {
        if (GuildaData.get(name) != null) return GuildaData.get(name).getGenerator_elixir_negro_level();
        else setOnCache(name);
        return GuildaData.get(name).getGenerator_elixir_negro_level();
    }

    public static int getGeneratorElixirLevel(String name) {
        if (GuildaData.get(name) != null) return GuildaData.get(name).getGenerator_elixir_level();
        else setOnCache(name);
        return GuildaData.get(name).getGenerator_elixir_level();
    }

    public static int getGeneratorCoinsLevel(String name) {
        if (GuildaData.get(name) != null) return GuildaData.get(name).getGenerator_coins_level();
        else setOnCache(name);
        return GuildaData.get(name).getGenerator_coins_level();
    }

    public static int getelixir_negro(String name) {
        if (GuildaData.get(name) != null) return GuildaData.get(name).getelixir_negro();
        else setOnCache(name);
        return GuildaData.get(name).getelixir_negro();
    }

    public static int getElixir(String name) {
        if (GuildaData.get(name) != null) return GuildaData.get(name).getElixir();
        else setOnCache(name);
        return GuildaData.get(name).getElixir();
    }

    public static int getCoins(String name) {
        if (GuildaData.get(name) != null) return GuildaData.get(name).getCoins();
        else setOnCache(name);
        return GuildaData.get(name).getCoins();
    }

    public static int getelixir_negroStockLevel(String name) {
        if (GuildaData.get(name) != null) return GuildaData.get(name).getelixir_negro_stock_level();
        else setOnCache(name);
        return GuildaData.get(name).getelixir_negro_stock_level();
    }

    public static int getElixirStockLevel(String name) {
        if (GuildaData.get(name) != null) return GuildaData.get(name).getElixir_stock_level();
        else setOnCache(name);
        return GuildaData.get(name).getElixir_stock_level();
    }

    public static int getCoinsStockLevel(String name) {
        if (GuildaData.get(name) != null) return GuildaData.get(name).getCoins_stock_level();
        else setOnCache(name);
        return GuildaData.get(name).getCoins_stock_level();
    }

    public static int getPlotLevel(String name) {
        if (GuildaData.get(name) != null) return GuildaData.get(name).getPlot_level();
        else setOnCache(name);
        return GuildaData.get(name).getPlot_level();
    }

    public static String getMembros(String name) {
        if (GuildaData.get(name) != null) return GuildaData.get(name).getMembros();
        else setOnCache(name);
        return GuildaData.get(name).getMembros();
    }

    public static void loadPlotSchem(String name) {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("SELECT * FROM Guilda_data WHERE name = ?");
            statement.setString(1, name);
            ResultSet rs = statement.executeQuery();

            /**
             * TODO * Check if this is correctly local to paste
             */
            while (rs.next()) {
                Blob blob = rs.getBlob("plot_schem");

                if (blob != null) {
                    File file = new File(Main.plugin.getDataFolder().getParent(), name + ".schem");
                    file.createNewFile();
                    FileOutputStream outputStream = new FileOutputStream(file);
                    InputStream inputStream = rs.getBlob("plot_schem").getBinaryStream();
                    byte[] buffer = new byte[inputStream.available()];
                    inputStream.read(buffer);
                    OutputStream outStream = new FileOutputStream(file);
                    outStream.write(buffer);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void updateLastElixirNegroCollected(String name) {
        Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, () -> {
            try {
                PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Guilda_data SET last_elixir_negro_collected = ? WHERE name = ?");
                statement.setString(2, name);
                statement.setLong(1, new java.util.Date().getTime());
                statement.executeUpdate();
                if (GuildaData.get(name) != null) GuildaData.get(name).setLast_elixir_negro_collected(new java.util.Date().getTime());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public static void updateLastElixirCollected(String name) {
        Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, () -> {
            try {
                PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Guilda_data SET last_elixir_collected = ? WHERE name = ?");
                statement.setString(2, name);
                statement.setLong(1, new java.util.Date().getTime());
                statement.executeUpdate();
                if (GuildaData.get(name) != null) GuildaData.get(name).setLast_elixir_collected(new java.util.Date().getTime());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public static void updateLastCoinsCollected(String name) {
        Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, () -> {
            try {
                PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Guilda_data SET last_coins_collected = ? WHERE name = ?");
                statement.setString(2, name);
                statement.setLong(1, new java.util.Date().getTime());
                statement.executeUpdate();
                if (GuildaData.get(name) != null) GuildaData.get(name).setLast_coins_collected(new java.util.Date().getTime());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public static void updateElixirNegro(String name, int elixir_negro) {
        Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, () -> {
            try {
                PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Guilda_data SET elixir_negro = ? WHERE name = ?");
                statement.setString(2, name);
                statement.setInt(1, elixir_negro);
                statement.executeUpdate();
                if (GuildaData.get(name) != null) GuildaData.get(name).setelixir_negro(elixir_negro);

                for (ServersList server : ServersList.values()) {
                    if (!server.name().replaceAll("_", "-").equals(Main.server_name))
                        RabbitMq.send(server.name().replaceAll("_", "-"), RMqChatManager.key + "/update_elixir_negro_guild=" + name + "=" + elixir_negro);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public static void updateElixir(String name, int elixir) {
        Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, () -> {
            try {
                PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Guilda_data SET elixir = ? WHERE name = ?");
                statement.setString(2, name);
                statement.setInt(1, elixir);
                statement.executeUpdate();
                if (GuildaData.get(name) != null) GuildaData.get(name).setElixir(elixir);

                for (ServersList server : ServersList.values()) {
                    if (!server.name().replaceAll("_", "-").equals(Main.server_name))
                        RabbitMq.send(server.name().replaceAll("_", "-"), RMqChatManager.key + "/update_elixir_guild=" + name + "=" + elixir);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public static void updateCoins(String name, int coins) {
        Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, () -> {
            try {
                PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Guilda_data SET coins = ? WHERE name = ?");
                statement.setString(2, name);
                statement.setInt(1, coins);
                statement.executeUpdate();
                if (GuildaData.get(name) != null) GuildaData.get(name).setCoins(coins);

                for (ServersList server : ServersList.values()) {
                    if (!server.name().replaceAll("_", "-").equals(Main.server_name))
                        RabbitMq.send(server.name().replaceAll("_", "-"), RMqChatManager.key + "/update_coins_guild=" + name + "=" + coins);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public static void updateAlocated(String name, String alocated) {
        Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, () -> {
            try {
                PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Guilda_data SET alocated = ? WHERE name = ?");
                statement.setString(2, name);
                statement.setString(1, alocated);
                statement.executeUpdate();
                if (GuildaData.get(name) != null) GuildaData.get(name).setAlocated(alocated);

                for (ServersList server : ServersList.values()) {
                    if (!server.name().replaceAll("_", "-").equals(Main.server_name))
                        RabbitMq.send(server.name().replaceAll("_", "-"), RMqChatManager.key + "/update_alocated_guild=" + name + "=" + alocated);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public static void updatePlotLevel(String name) {
        Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, () -> {
            try {
                PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Guilda_data SET plot_level = ? WHERE name = ?");
                statement.setString(2, name);
                statement.setInt(1, getPlotLevel(name) + 1);
                statement.executeUpdate();
                if (GuildaData.get(name) != null) GuildaData.get(name).setPlot_level(getPlotLevel(name) + 1);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public static void updateAttackCarried(String name) {
        Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, () -> {
            try {
                PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Guilda_data SET last_attack_carried = ? WHERE name = ?");
                statement.setString(2, name);
                statement.setDate(1, new Date(new java.util.Date().getTime()));
                statement.executeUpdate();
                if (GuildaData.get(name) != null) GuildaData.get(name).setLast_attack_carried(new java.util.Date());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public static void updateAttackSuffered(String name) {
        Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, () -> {
            try {
                PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Guilda_data SET last_attack_suffered = ? WHERE name = ?");
                statement.setString(2, name);
                statement.setDate(1, new Date(new java.util.Date().getTime()));
                statement.executeUpdate();
                if (GuildaData.get(name) != null) GuildaData.get(name).setLast_attack_suffered(new java.util.Date());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public static void updateLastPresence(String name) {
        Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, () -> {
            try {
                PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Guilda_data SET last_presence = ? WHERE name = ?");
                statement.setString(2, name);
                statement.setDate(1, new Date(new java.util.Date().getTime()));
                statement.executeUpdate();
                if (GuildaData.get(name) != null) GuildaData.get(name).setLast_presence(new java.util.Date());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public static void updateDefeats(String name, int defeats) {
        Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, () -> {
            try {
                PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Guilda_data SET defeats = ? WHERE name = ?");
                statement.setString(2, name);
                statement.setInt(1, defeats);
                statement.executeUpdate();
                if (GuildaData.get(name) != null) GuildaData.get(name).setDefeats(defeats);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public static void updateWins(String name, int wins) {
        Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, () -> {
            try {
                PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Guilda_data SET wins = ? WHERE name = ?");
                statement.setString(2, name);
                statement.setInt(1, wins);
                statement.executeUpdate();
                if (GuildaData.get(name) != null) GuildaData.get(name).setWins(wins);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public static void updateLevel(String name, int level) {
        Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, () -> {
            try {
                PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Guilda_data SET level = ? WHERE name = ?");
                statement.setString(2, name);
                statement.setInt(1, level);
                statement.executeUpdate();
                if (GuildaData.get(name) != null) GuildaData.get(name).setLevel(level);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public static void updateXp(String name, int xp) {
        Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, () -> {
            try {
                PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Guilda_data SET xp = ? WHERE name = ?");
                statement.setString(2, name);
                statement.setInt(1, xp);
                statement.executeUpdate();
                if (GuildaData.get(name) != null) GuildaData.get(name).setXp(xp);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public static void updateMembros(String name, String membros) {
        Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, () -> {
            try {
                PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Guilda_data SET membros = ? WHERE name = ?");
                statement.setString(2, name);
                statement.setString(1, membros);
                statement.executeUpdate();
                if (GuildaData.get(name) != null) GuildaData.get(name).setMembros(membros);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public static void updatePlotSchem(String name, File file) {
        Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, () -> {
            try {
                FileInputStream fileInput = new FileInputStream(file);
                PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Guilda_data SET plot_schem = ? WHERE name = ?");
                statement.setString(2, name);
                statement.setBlob(1, fileInput);
                statement.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        });
    }

    public static void setGuilda(String name, String uuid) {
        Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, () -> {
            try {
                PreparedStatement statement = MySql.connection.prepareStatement("INSERT INTO Guilda_data(name, membros, plot_level, coins_stock_level, elixir_stock_level, elixir_negro_stock_level, coins, elixir, elixir_negro, last_coins_collected, last_elixir_collected, last_elixir_negro_collected, generator_coins_level, generator_elixir_level, generator_elixir_negro_level, xp, level, wins, defeats, last_presence, last_attack_suffered, last_attack_carried, alocated) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
                statement.setString(1, name);
                statement.setString(2, uuid);
                statement.setInt(3, 1);
                statement.setInt(4, 1);
                statement.setInt(5, 0);
                statement.setInt(6, 0);
                statement.setInt(7, 0);
                statement.setInt(8, 0);
                statement.setInt(9, 0);
                statement.setLong(10, 0);
                statement.setLong(11, 0);
                statement.setLong(12, 0);
                statement.setInt(13, 1);
                statement.setInt(14, 0);
                statement.setInt(15, 0);
                statement.setInt(16, 0);
                statement.setInt(17, 1);
                statement.setInt(18, 0);
                statement.setInt(19, 0);
                statement.setDate(20, new Date(new java.util.Date().getTime()));
                statement.setTimestamp(21, null);
                statement.setTimestamp(22, null);
                statement.setString(23, null);
                statement.executeUpdate();

                if (GuildaData.get(name) != null && GuildaData.get(name).getMembros() == null)
                    GuildaData.get(name).delete();
                new GuildaData(name, uuid, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, new java.util.Date(), null, null, null).insert();
                setGuildaP(uuid, name, "ceo");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * table Guilda_player
     *
     * @param uuid
     * @return
     */
    public static String getGuildaP(String uuid) {
        if (GuildaPlayer.get(uuid) != null) return GuildaPlayer.get(uuid).getGuilda();
        else return getGuildaPDb(uuid);
    }

    public static String getRole(String uuid) {
        if (GuildaPlayer.get(uuid) != null) return GuildaPlayer.get(uuid).getRole();
        else return getRoleDb(uuid);
    }

    public static void removeGuildaP(String uuid) {
        Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, () -> {
            try {
                PreparedStatement statement = MySql.connection.prepareStatement("DELETE FROM Guilda_player WHERE uuid = ?");
                statement.setString(1, uuid);
                statement.executeUpdate();

                if (GuildaPlayer.get(uuid) != null) GuildaPlayer.get(uuid).delete();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public static void updateGuildaP(String uuid, String guilda) {
        Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, () -> {
            try {
                PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Guilda_player SET guilda = ? WHERE uuid = ?");
                statement.setString(2, uuid);
                statement.setString(1, guilda);
                statement.executeUpdate();

                if (GuildaPlayer.get(uuid) != null) GuildaPlayer.get(uuid).setGuilda(guilda);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public static void updateRole(String uuid, String role) {
        Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, () -> {
            try {
                PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Guilda_player SET role = ? WHERE uuid = ?");
                statement.setString(2, uuid);
                statement.setString(1, role);
                statement.executeUpdate();

                if (GuildaPlayer.get(uuid) != null) GuildaPlayer.get(uuid).setRole(role);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public static void setGuildaP(String uuid, String guilda, String role) {
        Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, () -> {
            try {
                PreparedStatement statement = MySql.connection.prepareStatement("INSERT INTO Guilda_player(uuid, guilda, role) VALUES (?, ?, ?)");
                statement.setString(1, uuid);
                statement.setString(2, guilda);
                statement.setString(3, role);
                statement.executeUpdate();

                if (GuildaPlayer.get(uuid) != null) new GuildaPlayer(uuid, guilda, role).insert();
                else GuildaPlayer.get(uuid).setGuilda(guilda);
                GuildaPlayer.get(uuid).setRole(role);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public static String getGuildaPDb(String uuid) {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("SELECT * FROM Guilda_player WHERE uuid = ?");
            statement.setString(1, uuid);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                String guilda = rs.getString("guilda");
                String role = rs.getString("role");
                if (GuildaPlayer.get(uuid) == null) new GuildaPlayer(uuid, guilda, role).insert();
                else GuildaPlayer.get(uuid).setGuilda(guilda);
                GuildaPlayer.get(uuid).setRole(role);
                return guilda;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        new GuildaPlayer(uuid, null, null).insert();
        return null;
    }

    public static String getRoleDb(String uuid) {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("SELECT * FROM Guilda_player WHERE uuid = ?");
            statement.setString(1, uuid);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                String guilda = rs.getString("guilda");
                String role = rs.getString("role");
                if (GuildaPlayer.get(uuid) == null) new GuildaPlayer(uuid, guilda, role).insert();
                else GuildaPlayer.get(uuid).setGuilda(guilda);
                GuildaPlayer.get(uuid).setRole(role);
                return role;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        new GuildaPlayer(uuid, null, null).insert();
        return null;
    }
}
