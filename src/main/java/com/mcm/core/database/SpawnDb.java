package com.mcm.core.database;

import com.mcm.core.MySql;
import com.mcm.core.cache.SpawnLoc;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SpawnDb {

    public static String getSpawn() {
        if (SpawnLoc.getSpawnloc() != null) {
            return SpawnLoc.getSpawnloc().getLocation();
        } else return getSpawnDb();
    }

    public static String getSpawnDb() {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("SELECT * FROM Spawn_loc WHERE servername = server");
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                String location = rs.getString("location");

                if (SpawnLoc.getSpawnloc() != null) {
                    SpawnLoc.getSpawnloc().setLocation(location);
                } else new SpawnLoc(location).insert();

                return location;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void setSpawn(String location) {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("INSERT INTO Spawn_loc(servername, location) VALUES (?, ?)");
            statement.setString(1, "server");
            statement.setString(2, location);
            statement.executeUpdate();

            if (SpawnLoc.getSpawnloc() != null) {
                SpawnLoc.getSpawnloc().setLocation(location);
            } else new SpawnLoc(location).insert();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateSpawn(String location) {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Spawn_loc SET location = ? WHERE servername = server");
            statement.setString(1, location);
            statement.executeUpdate();

            if (SpawnLoc.getSpawnloc() != null) {
                SpawnLoc.getSpawnloc().setLocation(location);
            } else new SpawnLoc(location).insert();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
