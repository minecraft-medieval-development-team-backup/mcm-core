package com.mcm.core.database;

import com.mcm.core.Main;
import com.mcm.core.MySql;
import com.mcm.core.cache.PlayerMine;
import com.mcm.core.cache.mines;
import com.mcm.scoreboard.utils.ScoreboardManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MinesDb {

    public static void createPlayerInfo(String uuid, int mine) {
        Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, () -> {
            try {
                PreparedStatement statement = MySql.connection.prepareStatement("INSERT INTO Player_mine_data(uuid, time, xp, level) VALUES (?, ?, ?, ?)");
                statement.setString(1, uuid + mine);
                statement.setInt(2, 0);
                statement.setInt(3, 0);
                statement.setInt(4, 1);
                statement.executeUpdate();

                new PlayerMine(uuid + mine, 0, 0, 1).insert();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public static boolean applyOnCache(String uuid, int mine) {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("SELECT * FROM Player_mine_data WHERE uuid = ?");
            statement.setString(1, uuid + mine);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                new PlayerMine(uuid + mine, rs.getLong("time"), rs.getInt("xp"), rs.getInt("level")).insert();
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        createPlayerInfo(uuid, mine);
        return false;
    }

    public static long getTime(String uuid, int mine) {
        if (PlayerMine.get(uuid + mine) != null) {
            return PlayerMine.get(uuid + mine).getTime();
        } else {
            applyOnCache(uuid, mine);
            return PlayerMine.get(uuid + mine).getTime();
        }
    }

    public static int getXP(String uuid, int mine) {
        if (PlayerMine.get(uuid + mine) != null) {
            return PlayerMine.get(uuid + mine).getXp();
        } else {
            applyOnCache(uuid, mine);
            return PlayerMine.get(uuid + mine).getXp();
        }
    }

    public static int getLevel(String uuid, int mine) {
        if (PlayerMine.get(uuid + mine) != null) {
            return PlayerMine.get(uuid + mine).getLevel();
        } else {
            applyOnCache(uuid, mine);
            return PlayerMine.get(uuid + mine).getLevel();
        }
    }

    public static void updateLevel(Player player, int mine, int level) {
        Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, () -> {
            try {
                PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Player_mine_data SET level = ? WHERE uuid = ?");
                statement.setString(2, player.getUniqueId().toString() + mine);
                statement.setInt(1, level);
                statement.executeUpdate();

                PlayerMine.get(player.getUniqueId().toString() + mine).setLevel(level);
                ScoreboardManager.updateAtual(player, mine);
                ScoreboardManager.updateProximo(player, mine);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public static void updateXP(Player player, int mine, int xp) {
        Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, () -> {
            try {
                PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Player_mine_data SET xp = ? WHERE uuid = ?");
                statement.setString(2, player.getUniqueId().toString() + mine);
                statement.setInt(1, xp);
                statement.executeUpdate();

                PlayerMine.get(player.getUniqueId().toString() + mine).setXp(xp);
                ScoreboardManager.updateAtual(player, mine);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public static void updateTime(String uuid, int mine, long time) {
        Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, () -> {
            try {
                PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Player_mine_data SET time = ? WHERE uuid = ?");
                statement.setString(2, uuid + mine);
                statement.setLong(1, time);
                statement.executeUpdate();

                PlayerMine.get(uuid + mine).setTime(time);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    //-----

    public static ArrayList<Integer> getIds() {
        return mines.mines;
    }

    public static int getLastIdDb() {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("SELECT MAX(id) as maxid FROM Mines_data");
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                return rs.getInt("maxid");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String getName(int id) {
        if (mines.get(id) != null) return mines.get(id).getName();
        else return null;
    }

    public static String getPos1(int id) {
        if (mines.get(id) != null) return mines.get(id).getPos1();
        else return null;
    }

    public static String getPos2(int id) {
        if (mines.get(id) != null) return mines.get(id).getPos2();
        else return null;
    }

    public static ArrayList<Location> getBlocks(int id) {
        if (mines.get(id) != null) return mines.get(id).getBlocks();
        else return null;
    }

    public static ArrayList<Location> getSpawns(int id) {
        if (mines.get(id) != null) return mines.get(id).getSpawns();
        else return null;
    }

    public static ArrayList<Material> getOres(int id) {
        if (mines.get(id) != null) return mines.get(id).getOres();
        else return null;
    }

    public static void createMine(String name, String pos1, String pos2, String blocksLoc, ArrayList<String> spawnsLoc, ArrayList<Material> ores) {
        Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, () -> {
            String s = null;
            for (String sp : spawnsLoc) {
                if (s == null) s = sp;
                else s += ":" + sp;
            }
            String o = null;
            for (Material or : ores) {
                if (o == null) o = or.name();
                else o += ":" + or.name();
            }

            try {
                PreparedStatement statement = MySql.connection.prepareStatement("INSERT INTO Mines_data(name, pos1, pos2, blocks, spawns, ores) VALUES (?, ?, ?, ?, ?, ?)");
                statement.setString(1, name);
                statement.setString(2, pos1);
                statement.setString(3, pos2);
                statement.setString(4, blocksLoc);
                statement.setString(5, s);
                statement.setString(6, o);
                statement.executeUpdate();

                ArrayList<Location> blocks = new ArrayList<>();
                if (blocksLoc.contains(":")) {
                    for (String split : blocksLoc.split(":")) {
                        String[] splt = split.split(",");
                        blocks.add(new Location(Bukkit.getWorld(splt[0]), Integer.valueOf(splt[1]), Integer.valueOf(splt[2]), Integer.valueOf(splt[3])));
                    }
                } else {
                    String[] split = blocksLoc.split(",");
                    blocks.add(new Location(Bukkit.getWorld(split[0]), Integer.valueOf(split[1]), Integer.valueOf(split[2]), Integer.valueOf(split[3])));
                }

                ArrayList<Location> spawns = new ArrayList<>();
                for (String spawn : spawnsLoc) {
                    String[] split = spawn.split(",");
                    spawns.add(new Location(Bukkit.getWorld(split[0]), Integer.valueOf(split[1]), Integer.valueOf(split[2]), Integer.valueOf(split[3]), Float.parseFloat(split[4]), Float.parseFloat(split[5])));
                }

                Bukkit.getScheduler().runTaskLaterAsynchronously(Main.plugin, () -> {
                    new mines(getLastIdDb(), name, pos1, pos2, blocks, spawns, ores).insert();
                }, 10L);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public static void setMinesOnCache() {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("SELECT * FROM Mines_data");
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                ArrayList<String> blocks = new ArrayList<>();
                String blcks = rs.getString("blocks");
                if (blcks.contains(":")) {
                    for (String split : blcks.split(":")) blocks.add(split);
                } else blocks.add(blcks);
                ArrayList<Location> blocksLoc = new ArrayList<>();
                for (String blk : blocks) {
                    String[] split = blk.split(",");
                    blocksLoc.add(new Location(Bukkit.getWorld(split[0]), Integer.valueOf(split[1]), Integer.valueOf(split[2]), Integer.valueOf(split[3])));
                }

                ArrayList<String> spawns = new ArrayList<>();
                String spws = rs.getString("spawns");
                if (spws.contains(":")) {
                    for (String split : spws.split(":")) spawns.add(split);
                } else spawns.add(spws);
                ArrayList<Location> spawnsLoc = new ArrayList<>();
                for (String sps : spawns) {
                    String[] split = sps.split(",");
                    spawnsLoc.add(new Location(Bukkit.getWorld(split[0]), Integer.valueOf(split[1]), Integer.valueOf(split[2]), Integer.valueOf(split[3]), Float.parseFloat(split[4]), Float.parseFloat(split[5])));
                }

                ArrayList<Material> ores = new ArrayList<>();
                String ors = rs.getString("ores");
                if (ors.contains(":")) {
                    for (String split : ors.split(":")) ores.add(Material.valueOf(split));
                } else ores.add(Material.valueOf(ors));

                new mines(rs.getInt("id"), rs.getString("name"), rs.getString("pos1"), rs.getString("pos2"), blocksLoc, spawnsLoc, ores).insert();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
