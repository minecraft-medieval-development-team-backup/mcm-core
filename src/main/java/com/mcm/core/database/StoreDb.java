package com.mcm.core.database;

import com.mcm.core.Main;
import com.mcm.core.MySql;
import com.mcm.core.cache.StoreSignPlayer;
import com.mcm.core.cache.StoreSignAdmin;
import com.mcm.core.cache.StoreGUI;
import com.mcm.core.utils.ItemStackSerializer;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.libs.org.apache.commons.lang3.RandomStringUtils;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class StoreDb {

    //------
    /*
     Store sign Player
     */
    //------

    public static void setStoreOnCache() {
        Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, new Runnable() {
            @Override
            public void run() {
                try {
                    PreparedStatement statement = MySql.connection.prepareStatement("SELECT * FROM Store_player_data");
                    ResultSet rs = statement.executeQuery();
                    while (rs.next()) {
                        new StoreSignPlayer(rs.getString("location"), rs.getString("uuid"), rs.getInt("quantity"), rs.getDouble("purchase"), rs.getDouble("sale")).insert();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static String getUUID(String location) {
        if (StoreSignPlayer.get(location) != null) return StoreSignPlayer.get(location).getUUID();
        return null;
    }

    public static double getSale(String location) {
        if (StoreSignPlayer.get(location) != null) return StoreSignPlayer.get(location).getSale();
        return 0.0;
    }

    public static double getPurchase(String location) {
        if (StoreSignPlayer.get(location) != null) return StoreSignPlayer.get(location).getPurchase();
        return 0.0;
    }

    public static int getQuantity(String location) {
        if (StoreSignPlayer.get(location) != null) return StoreSignPlayer.get(location).getQuantity();
        return 0;
    }

    public static String getStorePlayer(String location) {
        if (StoreSignPlayer.get(location) != null) return location;
        return null;
    }

    public static void createStorePlayer(String location, String uuid, int quantity, double purchase, double sale) {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("INSERT INTO Store_player_data(location, uuid, quantity, purchase, sale) VALUES (?, ?, ?, ?, ?)");
            statement.setString(1, location);
            statement.setString(2, uuid);
            statement.setInt(3, quantity);
            statement.setDouble(4, purchase);
            statement.setDouble(4, sale);
            statement.executeUpdate();

            new StoreSignPlayer(location, uuid, quantity, purchase, sale).insert();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void removeStorePlayer(String location) {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("DELETE FROM Store_player_data WHERE location = ?");
            statement.setString(1, location);
            statement.executeUpdate();

            if (StoreSignPlayer.get(location) != null) StoreSignPlayer.get(location).delete();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateSale(String location, double sale) {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Store_player_data SET sale = ? WHERE location = ?");
            statement.setString(2, location);
            statement.setDouble(1, sale);
            statement.executeUpdate();

            if (StoreSignPlayer.get(location) != null) StoreSignPlayer.get(location).updateSale(sale);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updatePurchase(String location, double purchase) {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Store_player_data SET purchase = ? WHERE location = ?");
            statement.setString(2, location);
            statement.setDouble(1, purchase);
            statement.executeUpdate();

            if (StoreSignPlayer.get(location) != null) StoreSignPlayer.get(location).updatePurchase(purchase);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateQuantity(String location, int quantity) {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Store_player_data SET quantity = ? WHERE location = ?");
            statement.setString(2, location);
            statement.setInt(1, quantity);
            statement.executeUpdate();

            if (StoreSignPlayer.get(location) != null) StoreSignPlayer.get(location).updateQuantity(quantity);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //------
    /*
     Store sign Admin
     */
    //------

    public static void setStoreAdminOnCache() {
        Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, new Runnable() {
            @Override
            public void run() {
                try {
                    PreparedStatement statement = MySql.connection.prepareStatement("SELECT * FROM Store_admin_data");
                    ResultSet rs = statement.executeQuery();
                    while (rs.next()) {
                        new StoreSignAdmin(rs.getString("location"), rs.getString("item"), rs.getInt("quantity"), rs.getDouble("purchase"), rs.getDouble("sale")).insert();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static String getItemAdmin(String location) {
        if (StoreSignAdmin.get(location) != null) return StoreSignAdmin.get(location).getItem();
        return null;
    }

    public static double getSaleAdmin(String location) {
        if (StoreSignAdmin.get(location) != null) return StoreSignAdmin.get(location).getSale();
        return 0.0;
    }

    public static double getPurchaseAdmin(String location) {
        if (StoreSignAdmin.get(location) != null) return StoreSignAdmin.get(location).getPurchase();
        return 0.0;
    }

    public static int getQuantityAdmin(String location) {
        if (StoreSignAdmin.get(location) != null) return StoreSignAdmin.get(location).getQuantity();
        return 0;
    }

    public static String getStoreAdmin(String location) {
        if (StoreSignAdmin.get(location) != null) return location;
        return null;
    }

    public static void createStoreAdmin(String location, int quantity, double purchase, double sale) {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("INSERT INTO Store_admin_data(location, item, quantity, purchase, sale) VALUES (?, ?, ?, ?, ?)");
            statement.setString(1, location);
            statement.setString(2, null);
            statement.setInt(3, quantity);
            statement.setDouble(4, purchase);
            statement.setDouble(5, sale);
            statement.executeUpdate();

            new StoreSignAdmin(location, null, quantity, purchase, sale).insert();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void removeStoreAdmin(String location) {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("DELETE FROM Store_admin_data WHERE location = ?");
            statement.setString(1, location);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (StoreSignAdmin.get(location) != null) StoreSignAdmin.get(location).delete();
    }

    public static void updateItem(String location, String item) {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Store_admin_data SET item = ? WHERE location = ?");
            statement.setString(2, location);
            statement.setString(1, item);
            statement.executeUpdate();

            if (StoreSignAdmin.get(location) != null) StoreSignAdmin.get(location).updateItem(item);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateSaleAdmin(String location, double sale) {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Store_admin_data SET sale = ? WHERE location = ?");
            statement.setString(2, location);
            statement.setDouble(1, sale);
            statement.executeUpdate();

            if (StoreSignAdmin.get(location) != null) StoreSignAdmin.get(location).updateSale(sale);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updatePurchaseAdmin(String location, double purchase) {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Store_admin_data SET purchase = ? WHERE location = ?");
            statement.setString(2, location);
            statement.setDouble(1, purchase);
            statement.executeUpdate();

            if (StoreSignAdmin.get(location) != null) StoreSignAdmin.get(location).updatePurchase(purchase);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateQuantityAdmin(String location, int quantity) {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Store_admin_data SET quantity = ? WHERE location = ?");
            statement.setString(2, location);
            statement.setInt(1, quantity);
            statement.executeUpdate();

            if (StoreSignAdmin.get(location) != null) StoreSignAdmin.get(location).updateQuantity(quantity);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //------
    /*
     Store GUI
     */
    //------

    public static List<String> categories() {
        return StoreGUI.categories;
    }

    public static void updateItemGUI(String category, String oldversion, String update) {
        for (int i = 0; i < StoreGUI.get(category).getItens().size(); i++) {
            if (StoreGUI.get(category).getItens() != null && StoreGUI.get(category).getItens().get(i) != null) {
                if (StoreGUI.get(category).getItens().get(i).equalsIgnoreCase(oldversion)) {
                    StoreGUI.get(category).getItens().set(i, update);

                    String newversion = null;
                    for (String itens : StoreGUI.get(category).getItens()) {
                        if (newversion == null) {
                            newversion = itens;
                        } else newversion += "/" + itens;
                    }

                    updateItemGUI(category, newversion);
                }
            }
        }
    }

    public static List<String> getItensGUI(String category) {
        return StoreGUI.get(category).getItens();
    }

    public static ItemStack getCategoryItemGUI(String category) {
        return StoreGUI.get(category).getCategoryItem();
    }

    private static void updateItemGUI(String category, String update) {
        Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, new Runnable() {
            @Override
            public void run() {
                try {
                    PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Store_data SET itens = ? WHERE category = ?");
                    statement.setString(2, category);
                    statement.setString(1, update);
                    statement.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static void updateCategoryItemGUI(String category, ItemStack categoryItem) {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Store_data SET categoryItem = ? WHERE category = ?");
            statement.setString(2, category);
            statement.setString(1, ItemStackSerializer.serialize(categoryItem));
            statement.executeUpdate();
            StoreGUI.get(category).updateCategoryItem(categoryItem);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void removeItemOnCategoryGUI(String category, String item) {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Store_data SET itens = ? WHERE category = ?");
            statement.setString(2, category);
            String itens = null;
            List<String> i = StoreGUI.get(category).getItens();
            for (String s : i) {
                itens += "/" + s;
            }
            statement.setString(1, itens.replace(item, "").replaceAll("//", ""));
            StoreGUI.get(category).removeItem(item);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static boolean rmvItemOnGUIwithItemID(String itemID) {
        try {
            String category = null;
            if (StoreGUI.categories != null) {
                for (String categories : StoreGUI.categories) {
                   for (String id : StoreGUI.get(categories).getIds()) {
                       if (id.equalsIgnoreCase(itemID)) {
                           category = categories;
                       }
                   }
                }
            }
            if (category == null) return false;

            PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Store_data SET itens = ? WHERE category = ?");
            statement.setString(2, category);
            String itens = null;
            List<String> i = StoreGUI.get(category).getItens();
            String item = null;
            for (String s : i) {
                if (!s.contains(itemID)) {
                    itens += "/" + s;
                } else item = s;
            }
            if (item == null) return false;
            statement.setString(1, itens.replaceAll("//", ""));
            StoreGUI.get(category).removeItem(item);

            for (Player target : Bukkit.getOnlinePlayers()) {
                if (target.getOpenInventory() != null) {
                    if (target.getOpenInventory().getTitle().equalsIgnoreCase(com.mcm.core.Main.getTradution("8G6YG#g6VM!VYgm", target.getUniqueId().toString()))) {
                        target.getOpenInventory().close();
                    }
                }
            }

            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void addItemOnCategoryGUI(String category, String item) {
        try {
            String random = RandomStringUtils.random( 6, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789");
            PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Store_data SET itens = ? WHERE category = ?");
            statement.setString(2, category);
            if (StoreGUI.get(category) != null && !StoreGUI.get(category).getItens().isEmpty()) {
                String itens = null;
                for (String i : StoreGUI.get(category).getItens()) {
                    if (itens == null) itens = i; else if (itens != null) itens += "/" + i;
                }
                statement.setString(1, itens + "/" + item + ":#" + random);
            } else  statement.setString(1, item + ":#" + random);
            statement.executeUpdate();
            StoreGUI.get(category).addItem(item);
            StoreGUI.get(category).addID(random);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void deleteCategoryGUI(String category) {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("DELETE FROM Store_data WHERE category = ?");
            statement.setString(1, category);
            statement.executeUpdate();
            StoreGUI.get(category).deleteCategory();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void createNewCategoryGUI(String category, ItemStack itemCategory) {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("INSERT INTO Store_data(category, categoryItem, itens) VALUES (?, ?, ?)");
            statement.setString(1, category);
            String a = ItemStackSerializer.serialize(itemCategory);
            statement.setString(2, a);
            statement.setString(3, null);
            statement.executeUpdate();
            new StoreGUI(category, itemCategory, new ArrayList<>()).insert();
            if (StoreGUI.categories == null) StoreGUI.categories = new ArrayList<>();
            StoreGUI.categoriesAdd(category);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void setStoreInCacheGUI() {
        setCategoriesGUI();
        if (categories() != null) {
            for (String category : categories()) {
                try {
                    PreparedStatement statement = MySql.connection.prepareStatement("SELECT * FROM Store_data WHERE category = ?");
                    statement.setString(1, category);
                    ResultSet rs = statement.executeQuery();
                    while (rs.next()) {
                        String i = rs.getString("itens");
                        List<String> itens = new ArrayList<>();
                        if (i != null) {
                            if (i.contains("/")) {
                                String[] split = i.split("/");
                                for (String s : split) {
                                    itens.add(s);
                                }
                            } else {
                                itens.add(i);
                            }
                        }

                        new StoreGUI(category, ItemStackSerializer.deserialize(getItemCategoryDataGUI(category)), itens).insert();
                        for (String item : itens) {
                            StoreGUI.get(category).addID(item.split(":")[6]);
                        }
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static String getItemCategoryDataGUI(String category) {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("SELECT * FROM Store_data WHERE category = ?");
            statement.setString(1, category);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                return rs.getString("categoryItem");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static void setCategoriesGUI() {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("SELECT * FROM Store_data");
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                String category = rs.getString("category");
                if (StoreGUI.categories == null) StoreGUI.categories = new ArrayList<>();
                StoreGUI.categoriesAdd(category);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //------
    /*
     Store log
     */
    //------

    public static void setLog(String uuid, String item) {
        Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, new Runnable() {
            @Override
            public void run() {
                try {
                    PreparedStatement statement = MySql.connection.prepareStatement("INSERT INTO Store_log(uuid, item, hour, shop) VALUES (?, ?, ?, ?)");
                    statement.setString(1, uuid);
                    statement.setString(2, item);
                    statement.setDate(3, new Date(new java.util.Date().getTime()));
                    statement.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
