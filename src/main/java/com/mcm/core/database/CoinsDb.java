package com.mcm.core.database;

import com.mcm.core.MySql;
import com.mcm.core.cache.Coins;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class CoinsDb {

    public static int getCoins(String uuid) {
        if (Coins.get(uuid) != null) {
            return Coins.get(uuid).getCoins();
        } else if (getUuidExistDb(uuid) == null) set(uuid); else return getCoinsDb(uuid);
        return 0;
    }

    public static int getCoinsDb(String uuid) {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("SELECT * FROM Coins_data WHERE uuid = ?");
            statement.setString(1, uuid);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                int coins = rs.getInt("coins");
                new Coins(uuid, coins).insert();
                return coins;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String getUuidExistDb(String uuid) {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("SELECT * FROM Coins_data WHERE uuid = ?");
            statement.setString(1, uuid);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                return rs.getString("uuid");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void delete(String uuid) {
        Coins.cache.remove(uuid);
    }

    public static void updateCoins(String uuid, int coins) {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Coins_data SET coins = ? WHERE uuid = ?");
            statement.setString(2, uuid);
            statement.setInt(1, coins);
            statement.executeUpdate();

            if (Coins.get(uuid) != null) {
                Coins.get(uuid).setCoins(coins);
            } else new Coins(uuid, coins).insert();
            Player player = Bukkit.getPlayer(UUID.fromString(uuid));
            if (player != null && player.isOnline()) com.mcm.scoreboard.utils.ScoreboardManager.updateCoins(player);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void set(String uuid) {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("INSERT INTO Coins_data(uuid, coins) VALUES (?, ?)");
            statement.setString(1, uuid);
            statement.setInt(2, 0);
            statement.executeUpdate();

            if (Coins.get(uuid) != null) {
                Coins.get(uuid).setCoins(0);
            } else new Coins(uuid, 0).insert();

            Player player = Bukkit.getPlayer(UUID.fromString(uuid));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
