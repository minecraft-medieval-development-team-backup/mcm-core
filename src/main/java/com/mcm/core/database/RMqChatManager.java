package com.mcm.core.database;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.mcm.core.Main;
import com.mcm.core.RabbitMq;
import com.mcm.core.Redis;
import com.mcm.core.cache.*;
import com.mcm.core.enums.ServersList;
import com.mcm.core.serverswitch.PlayerSwitch;
import com.mcm.core.utils.ItemStackSerializer;
import com.mcm.scoreboard.utils.ScoreboardManager;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.DeliverCallback;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.TimeoutException;

public class RMqChatManager {

    public static String key = "R&9VQ57Bfq6uuwm?wA@cHANWdEhHt*";

    public static void syncChat() {
        sync(Main.server_name);
    }

    private static void sync(String channel_name) {
        try {
            Connection connection = RabbitMq.factory.newConnection();
            Channel channel = connection.createChannel();

            channel.queueDeclare(channel_name, true, false, true, null);
            DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                String message = new String(delivery.getBody(), "UTF-8");

                String[] perm_chat_staff = {"superior", "gestor", "moderador", "suporte"};

                //RMqChatManager.key + "/update_coins_guild=" + name + "=" + coins
                if (message.contains(key + "/update_plot_level_guild=")) {
                    String[] split = message.split("=");

                    if (GuildaData.get(split[1]) != null) {
                        GuildaData.get(split[1]).setPlot_level(Integer.valueOf(split[2]));
                        if (split[3].equals("coins")) {
                            GuildaData.get(split[1]).setCoins(Integer.valueOf(split[4]));
                        } else if (split[3].equals("elixir")) {
                            GuildaData.get(split[1]).setElixir(Integer.valueOf(split[4]));
                        } else if (split[3].equals("elixir_negro")) {
                            GuildaData.get(split[1]).setelixir_negro(Integer.valueOf(split[4]));
                        }

                        for (Player target : Bukkit.getOnlinePlayers()) {
                            if (GuildaDb.getGuildaP(target.getUniqueId().toString()).equals(split[1])) {
                                target.sendMessage(Main.getTradution("33xUA$gSTbJhmD*", target.getUniqueId().toString()) + GuildaDb.getPlotLevel(GuildaDb.getGuildaP(target.getUniqueId().toString())) + " \n ");
                                target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                target.playSound(target.getLocation(), Sound.ITEM_TRIDENT_THUNDER, 1.5f, 1.5f);
                            }
                        }
                    }
                } else if (message.contains(key + "/update_elixir_negro_guild=")) {
                    String[] split = message.split("=");

                    if (GuildaData.get(split[1]) != null) GuildaData.get(split[1]).setelixir_negro(Integer.valueOf(split[2]));
                } else if (message.contains(key + "/update_elixir_guild=")) {
                    String[] split = message.split("=");

                    if (GuildaData.get(split[1]) != null) GuildaData.get(split[1]).setElixir(Integer.valueOf(split[2]));
                } else if (message.contains(key + "/update_coins_guild=")) {
                    String[] split = message.split("=");

                    if (GuildaData.get(split[1]) != null) GuildaData.get(split[1]).setCoins(Integer.valueOf(split[2]));
                } else if (message.contains(key + "/update_alocated_guild=")) {
                    String[] split = message.split("=");

                    if (GuildaData.get(split[1]) != null) GuildaData.get(split[1]).setAlocated(split[2]);
                } else if (message.contains(key + "/user_exited_guild=")) {
                    String[] split = message.split("=");

                    if (GuildaData.get(split[1]) != null) GuildaData.get(split[1]).setMembros(split[3]);
                    for (Player target : Bukkit.getOnlinePlayers()) {
                        if (GuildaDb.getGuildaP(target.getUniqueId().toString()).equals(split[1])) {
                            target.sendMessage(ChatColor.GREEN + " \n * " + split[2] + Main.getTradution("Vf!$nk*SFBHs$B5", target.getUniqueId().toString()));
                            target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    }
                } else if (message.contains(key + "/user_removed_guild=")) {
                    String[] split = message.split("=");

                    if (GuildaData.get(split[1]) != null) GuildaData.get(split[1]).setMembros(split[3]);
                    for (Player target : Bukkit.getOnlinePlayers()) {
                        if (target.getName().equals(split[2])) {
                            target.sendMessage(Main.getTradution("Y3D$rA&!NVjKEv@", target.getUniqueId().toString()));
                            target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            target.playSound(target.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                        if (GuildaDb.getGuildaP(target.getUniqueId().toString()).equals(split[1])) {
                            target.sendMessage(ChatColor.GREEN + " \n * " + split[2] + Main.getTradution("94px?3BjrXyCTS?", target.getUniqueId().toString()));
                            target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    }
                } else if (message.contains(key + "/new_user_guild=")) {
                    String[] split = message.split("=");

                    if (GuildaData.get(split[1]) != null) GuildaData.get(split[1]).setMembros(split[2]);
                    for (Player player : Bukkit.getOnlinePlayers()) {
                        if (GuildaDb.getGuildaP(player.getUniqueId().toString()).equals(split[1])) {
                            player.sendMessage(ChatColor.GREEN + " \n * " + split[3] + Main.getTradution("4PKcZCn2?QRr*WR", player.getUniqueId().toString()));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    }
                } else if (message.contains(key + "/invite_guild=")) {
                    String[] split = message.split("=");
                    String name = split[1];
                    Player player = Bukkit.getPlayerExact(split[2]);

                    if (player != null && player.isOnline()) {
                        player.sendMessage(Main.getTradution("mT@QSP6*vv@SC*5", player.getUniqueId().toString()) + name + Main.getTradution("z?brZYW56A5SjBH", player.getUniqueId().toString()) + name + "\n ");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        if (InvitesGuilda.get(player.getUniqueId().toString()) == null)
                            new InvitesGuilda(player.getUniqueId().toString()).insert().add(name);
                        else InvitesGuilda.get(player.getUniqueId().toString()).add(name);
                    }
                } else if (message.contains(key + "/item_sold_market=")) {
                    String[] split = message.split("=");
                    String id = split[1];
                    String uuid = id.substring(0, 36);

                    for (Player player : Bukkit.getOnlinePlayers()) {
                        if (player.getUniqueId().toString().equals(uuid)) {
                            Coins.get(uuid).setCoins(Coins.get(uuid).getCoins() + (int) ItensMercado.get(id).getPrice());
                            ScoreboardManager.updateCoins(player);
                        }
                    }

                    MercadoDb.removeItem(id, true);
                } else if (message.contains(key + "/publishing=")) {
                    String[] split = message.split("=");

                    for (Player player : Bukkit.getOnlinePlayers()) {
                        player.sendMessage(Main.getTradution(split[1], player.getUniqueId().toString()) + split[2] + Main.getTradution(split[3], player.getUniqueId().toString()) + split[4] + Main.getTradution(split[5], player.getUniqueId().toString()) + split[5] + Main.getTradution(split[6], player.getUniqueId().toString()));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else if (message.contains(key + "/add_item_on_mercado=")) {
                    String[] split = message.split("=");

                    Date date = new Date();
                    date.setTime(Long.parseLong(split[6]));
                    Mercado.get(split[3]).addItem(new ItensMercado(split[1].substring(0, 35), split[2].substring(36), split[2], split[3], ItemStackSerializer.deserialize(split[4]), Double.parseDouble(split[5]), date).insert());
                } else if (message.contains(key + "/remove_item_on_mercado=")) {
                    String[] split = message.split("=");
                    MercadoDb.removeItem(split[1], true);
                } else if (message.contains(key + "/staff_entry=")) {
                    String[] split = message.split("=");
                    ScoreboardManager.addOnTab(split[2], split[1]);
                } else if (message.contains(key + "/tpaaccept=")) {
                    String[] split = message.split("=");
                    Player player = Bukkit.getPlayerExact(split[1]);
                    String uuid = player.getUniqueId().toString();
                    if (player != null && player.isOnline()) {
                        try {
                            String server = split[2];
                            ByteArrayDataOutput out = ByteStreams.newDataOutput();
                            out.writeUTF("Connect");
                            out.writeUTF(server);

                            player.sendPluginMessage(Main.plugin, "BungeeCord", out.toByteArray());
                            player.sendMessage(Main.getTradution("k?6Xx%dzKJ?RTpR", player.getUniqueId().toString()) + split[3] + Main.getTradution("64&nR&8ct5?%v54", player.getUniqueId().toString()));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, 1.0f, 1.0f);
                            Redis.localhost.set(uuid + "tpa", split[3]);
                            PlayerSwitch.sendData(player, server);
                            if (TpaSended.get(player.getUniqueId().toString()) != null)
                                TpaSended.get(player.getUniqueId().toString()).delete();
                        } catch (Exception e) {
                            player.sendMessage(Main.getTradution("y?C7uWPAkgs@hdv", player.getUniqueId().toString()));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    }
                } else if (message.contains(key + "/tpaexpired=")) {
                    String[] split = message.split("=");
                    Player player = Bukkit.getPlayerExact(split[1]);
                    if (player != null && player.isOnline()) {
                        String[] msg = split[2].split("///");
                        player.sendMessage(Main.getTradution(msg[0], player.getUniqueId().toString()) + msg[1] + Main.getTradution(msg[2], player.getUniqueId().toString()));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        if (TpaSended.get(player.getUniqueId().toString()) != null)
                            TpaSended.get(player.getName()).removeList(msg[1]);
                    }
                } else if (message.contains(key + "/tpa_solicite=")) {
                    String[] split = message.split("=");
                    Player player = Bukkit.getPlayerExact(split[1]);
                    if (player != null && player.isOnline()) {
                        String[] msg = split[2].split("///");
                        player.sendMessage(Main.getTradution(msg[0], player.getUniqueId().toString()) + msg[1] + Main.getTradution(msg[2], player.getUniqueId().toString()) + msg[3]);
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                        System.out.println(split[3]);
                        if (TpaInvitations.get(player.getName()) != null)
                            TpaInvitations.get(player.getName()).addList(split[3]);
                        else new TpaInvitations(player.getName()).insert().addList(split[3]);
                        TpaExpired.addNewUser(player.getName(), split[3]);
                    }
                } else if (message.contains(key + "/tp=")) {
                    String[] split = message.split("=");
                    Player player = Bukkit.getPlayerExact(split[1]);

                    if (player != null && player.isOnline()) {
                        try {
                            String server = PlayerLocation.get(split[2]);
                            ByteArrayDataOutput out = ByteStreams.newDataOutput();
                            out.writeUTF("Connect");
                            out.writeUTF(server);

                            player.sendPluginMessage(Main.plugin, "BungeeCord", out.toByteArray());
                            player.sendMessage(Main.getTradution("fR9%Sbnj?7JXr*9", player.getUniqueId().toString()) + split[2]);
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, 1.0f, 1.0f);
                            Redis.localhost.set(player.getUniqueId().toString() + "tp", split[2]);
                            PlayerSwitch.sendData(player, server);
                        } catch (Exception e) {
                            player.sendMessage(Main.getTradution("y?C7uWPAkgs@hdv", player.getUniqueId().toString()));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    }
                } else if (message.contains(key + "/server_online=")) {
                    String[] split = message.split("=");
                    if (split[1].equals("true")) {
                        new OnlineCount(split[2], 0).insert();
                        RabbitMq.send(split[2], key + "/online_update=" + Main.server_name + "=" + Bukkit.getOnlinePlayers().size());
                    } else OnlineCount.get(split[2]).delete();
                } else if (message.contains(key + "/online_update=")) {
                    String[] split = message.split("=");
                    if (OnlineCount.get(split[1]) == null)
                        new OnlineCount(split[1], Integer.valueOf(split[2])).insert();
                    else OnlineCount.get(split[1]).setCount(Integer.valueOf(split[2]));
                    for (Player target : Bukkit.getOnlinePlayers()) ScoreboardManager.updateOnline(target);
                } else if (message.contains(key + "/chat_status=")) {
                    if (message.contains("true")) {
                        ChatStatus.setStatus(true);
                    } else ChatStatus.setStatus(false);
                } else if (message.contains(key + "/chat_clear")) {
                    for (Player target : Bukkit.getOnlinePlayers()) {
                        for (int i = 0; i < 100; i++) {
                            target.sendMessage(" ");
                        }
                        target.sendMessage(Main.getTradution("mn8!dpYXT8KJMRT", target.getUniqueId().toString()));
                    }
                } else if (message.contains(key + "/chat_staff=")) {
                    for (Player target : Bukkit.getOnlinePlayers()) {
                        if (Arrays.asList(perm_chat_staff).contains(com.mcm.core.database.TagDb.getTag(target.getUniqueId().toString()))) {
                            target.sendMessage(message.split("=")[1]);
                        }
                    }
                } else if (message.contains(key + "/tell=")) {
                    String[] split = message.split("=");
                    Player target = Bukkit.getPlayerExact(split[1]);
                    if (target != null && target.isOnline()) {
                        for (ServersList server : ServersList.values()) {
                            if (!com.mcm.core.Main.server_name.equals(server.name().replaceAll("_", "-")))
                                RabbitMq.send(server.name().replaceAll("_", "-"), key + "/tell_received=" + target.getName());
                        }
                        target.sendMessage(split[2]);
                        target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else if (message.contains(key + "/tell_received=")) {
                    String[] split = message.split("=");
                    new TellReceived(split[1]).insert();
                } else {
                    for (Player target : Bukkit.getOnlinePlayers()) {
                        if (com.mcm.core.database.ConfigDb.get(target.getUniqueId().toString()).getChatGlobal()) {
                            target.sendMessage(message);
                        }
                    }
                }
            };
            channel.basicConsume(channel_name, true, deliverCallback, consumerTag -> {
            });
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
    }
}
