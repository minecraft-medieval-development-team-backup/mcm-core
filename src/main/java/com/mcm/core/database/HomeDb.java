package com.mcm.core.database;

import com.mcm.core.Main;
import com.mcm.core.MySql;
import com.mcm.core.cache.Home;
import org.bukkit.Bukkit;

import java.io.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class HomeDb {

    public static void createHome(String uuid, int size) {
        Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, () -> {
            try {
                PreparedStatement statement = MySql.connection.prepareStatement("INSERT INTO Home_data(uuid, size, friends, move_perm) VALUES (?, ?, ?, ?)");
                statement.setString(1, uuid);
                statement.setInt(2, size);
                statement.setString(3, null);
                statement.setBoolean(4, true);
                statement.executeUpdate();

                new Home(uuid, size, new ArrayList<>(), true).insert();
                if (Home.get(uuid + "1234") != null) Home.get(uuid + "1234").delete();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public static boolean haveHome(String uuid) {
        if (Home.get(uuid + "1234") != null) return false;
        setOnCache(uuid);
        if (Home.get(uuid + "1234") != null) return false; else return true;
    }

    public static int getSize(String uuid) {
        if (Home.get(uuid) == null) {
            if (setOnCache(uuid)) return Home.get(uuid).getSize();
        } else return Home.get(uuid).getSize();
        return 0;
    }

    public static boolean getMovePerm(String uuid) {
        if (Home.get(uuid) == null) {
            if (setOnCache(uuid)) return Home.get(uuid).isMove_perm();
        } else return Home.get(uuid).isMove_perm();
        return true;
    }

    public static ArrayList<String> getFriends(String uuid) {
        if (Home.get(uuid) == null) {
            if (setOnCache(uuid)) return Home.get(uuid).getFriends(); else return null;
        } else return Home.get(uuid).getFriends();
    }

    public static boolean containsFriend(String uuid, String uuid_friend) {
        if (Home.get(uuid) == null) {
            if (setOnCache(uuid)) return Home.get(uuid).getFriends().contains(uuid_friend);
        } else return Home.get(uuid).getFriends().contains(uuid_friend);
        return false;
    }

    public static void addFriend(String uuid, String uuid_friend) {
        Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, () -> {
            try {
                String friends = null;
                for (String f : Home.get(uuid).getFriends()) {
                    if (friends == null) friends = f;
                    else friends += ":" + f;
                }
                PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Home_data SET friends = ? WHERE uuid = ?");
                statement.setString(2, uuid);
                if (friends == null) {
                    statement.setString(1, uuid_friend);
                } else statement.setString(1, friends + ":" + uuid_friend);
                statement.executeUpdate();

                Home.get(uuid).addFriend(uuid_friend);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public static void removeFriend(String uuid, String uuid_friend) {
        Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, () -> {
            try {
                String friends = null;
                for (String f : Home.get(uuid).getFriends()) {
                    if (f.equals(uuid_friend)) continue;
                    if (friends == null) friends = f;
                    else friends += ":" + f;
                }
                PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Home_data SET friends = ? WHERE uuid = ?");
                statement.setString(2, uuid);
                statement.setString(1, uuid_friend);
                statement.executeUpdate();

                Home.get(uuid).removeFriend(uuid_friend);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public static boolean setOnCache(String uuid) {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("SELECT * FROM Home_data WHERE uuid = ?");
            statement.setString(1, uuid);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                ArrayList<String> friends = new ArrayList<>();
                String f = rs.getString("friends");
                if (f != null) {
                    for (String fr : f.split(":")) {
                        friends.add(fr);
                    }
                }
                new Home(uuid, rs.getInt("size"),  friends, rs.getBoolean("move_perm")).insert();
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        new Home(uuid + "1234", 0, new ArrayList<>(), true).insert();
        return false;
    }

    //----

    public static void putSchem(String uuid) {
        Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, () -> {
            try {
                File file = new File("plot-schems/" + uuid + ".schematic");
                FileInputStream fileInput = new FileInputStream(file);
                PreparedStatement statement = MySql.connection.prepareStatement("INSERT INTO Schem_home(uuid, blob_data) VALUES (?, ?)");
                statement.setString(1, uuid);
                statement.setBlob(2, fileInput);
                statement.executeUpdate();
            } catch (SQLException | FileNotFoundException e) {
                e.printStackTrace();
            }
        });
    }

    public static void loadSchem(String uuid) {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("SELECT * FROM Schem_home WHERE uuid = ?");
            statement.setString(1, uuid);
            ResultSet rs = statement.executeQuery();

            File file = new File(Main.plugin.getDataFolder().getParent(), uuid + ".schematic");
            file.createNewFile();
            while (rs.next()) {
                InputStream inputStream = rs.getBlob("blob_data").getBinaryStream();
                byte[] buffer = new byte[inputStream.available()];
                inputStream.read(buffer);
                OutputStream outStream = new FileOutputStream(file);
                outStream.write(buffer);
            }
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
    }
}
