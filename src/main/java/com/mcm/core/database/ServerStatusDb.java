package com.mcm.core.database;

import com.mcm.core.*;
import com.mcm.core.enums.ServersList;
import lombok.val;
import org.bukkit.Bukkit;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ServerStatusDb {

    public static String getServerName(String ip) {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("SELECT * FROM Server_status WHERE ip = ?");
            statement.setString(1, ip);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                return rs.getString("server_name");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        create(ip);
        return null;
    }

    public static void create(String ip) {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("INSERT INTO Server_status(ip) VALUES (?)");
            statement.setString(1, ip);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateStatus(String ip, boolean status) {
        if (status == false) setOnlineOnDisable(ip);
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Server_status SET status = ? WHERE ip = ?");
            statement.setString(2, ip);
            statement.setBoolean(1, status);
            statement.executeUpdate();
            for (ServersList server: ServersList.values()) {
                if (!server.name().replaceAll("_", "-").equals(Main.server_name)) {
                    if (status == true) RabbitMq.send(server.name().replaceAll("_", "-"), RMqChatManager.key + "/server_online=true=" + Main.server_name);
                    else RabbitMq.send(server.name().replaceAll("_", "-"), RMqChatManager.key + "/server_online=false=" + Main.server_name);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateOnline(String ip, int online) {
        Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, new Runnable() {
            @Override
            public void run() {
                try {
                    PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Server_status SET online = ? WHERE ip = ?");
                    statement.setString(2, ip);
                    statement.setInt(1, online);
                    statement.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static void setOnlineOnDisable(String ip) {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Server_status SET online = ? WHERE ip = ?");
            statement.setString(2, ip);
            statement.setInt(1, 0);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
