package com.mcm.core.database;

import java.util.HashMap;

public class ConfigDb {

    private static String uuid;
    private static boolean chatLocal;
    private static boolean chatGlobal;
    private static boolean deposits;
    private static boolean teleports;
    private static boolean fly;
    private static boolean invisible;
    private static boolean adverts;
    private static HashMap<String, ConfigDb>  cache = new HashMap<>();

    public ConfigDb(String uuid) {
        this.uuid = uuid;
        this.chatLocal = true;
        this.chatGlobal = true;
        this.deposits = true;
        this.teleports = true;
        this.fly = false;
        this.invisible = false;
        this.adverts = true;
    }

    public ConfigDb insert() {
        this.cache.put(this.uuid, this);
        return this;
    }

    public static ConfigDb get(String uuid) {
        if (cache.get(uuid) == null) new ConfigDb(uuid).insert();
        return cache.get(uuid);
    }

    public boolean getAdverts() {
        return this.adverts;
    }

    public void setAdverts(boolean adverts) {
        this.adverts = adverts;
    }

    public boolean getInvisible() {
        return this.invisible;
    }

    public void setInvisible(boolean invisible) {
        this.invisible = invisible;
    }

    public boolean getFly() {
        return this.fly;
    }

    public void setFly(boolean fly) {
        this.fly = fly;
    }

    public boolean getTeleports() {
        return this.teleports;
    }

    public void setTeleports(boolean teleports) {
        this.teleports = teleports;
    }

    public boolean getDeposits() {
        return this.deposits;
    }

    public void setDeposits(boolean deposits) {
        this.deposits = deposits;
    }

    public boolean getChatGlobal() {
        return this.chatGlobal;
    }

    public void setChatGlobal(boolean chatGlobal) {
        this.chatGlobal = chatGlobal;
    }

    public boolean getChatLocal() {
        return this.chatLocal;
    }

    public void setChatLocal(boolean chatLocal) {
        this.chatLocal = chatLocal;
    }
}
