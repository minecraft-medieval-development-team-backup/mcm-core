package com.mcm.core.serverswitch;

import com.mcm.core.Main;
import com.mcm.core.cache.isSwitch;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.potion.PotionEffect;

import java.util.HashMap;

public class PlayerJoinEvents implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        String uuid = player.getUniqueId().toString();

        deleteData(player);
        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
            @Override
            public void run() {
                HashMap<String, Player_data> data = DataManager.getData(uuid);
                boolean received = data != null;
                if (received == true) {
                    applyData(player, uuid, data);
                    new isSwitch(uuid).insert();
                } else {
                    applyData(player, uuid, SerializerData.deserialize(PlayerDataDb.getData(uuid)));
                }
            }
        }, 40L);
    }

    private static void applyData(Player player, String uuid, HashMap<String, Player_data> data) {
        player.getInventory().setStorageContents(data.get(uuid).getItens());
        player.getInventory().setArmorContents(data.get(uuid).getArmors());
        player.getEnderChest().setContents(data.get(uuid).getEnderChest());
        player.getInventory().setItemInOffHand(data.get(uuid).getLeftHand());
        player.setFireTicks(data.get(uuid).getFireTicks());
        player.setAllowFlight(data.get(uuid).isAllowFlight());
        player.setWalkSpeed(data.get(uuid).getWalkSpeed());
        player.setLevel(data.get(uuid).getLevel());
        player.setExp(data.get(uuid).getExp());
        player.setFoodLevel(data.get(uuid).getFoodLevel());
        player.setHealthScale(data.get(uuid).getHealthScale());
        player.setHealth(data.get(uuid).getHealth());
        player.setGameMode(data.get(uuid).getGameMode());
        player.addPotionEffects(data.get(uuid).getPotionEffects());
        player.updateInventory();
        DataManager.deleteData(uuid);
    }

    private static void deleteData(Player player) {
        player.getInventory().clear();
        player.setFireTicks(0);
        player.setAllowFlight(false);
        player.setWalkSpeed(0.2f);
        player.setFlySpeed(0.2f);
        player.setLevel(0);
        player.setExp(0);
        player.setFoodLevel(20);
        player.setHealth(20);
        player.setGameMode(GameMode.SURVIVAL);
        for (PotionEffect potionEffect : player.getActivePotionEffects()) {
            player.removePotionEffect(potionEffect.getType());
        }
        player.updateInventory();
    }
}
