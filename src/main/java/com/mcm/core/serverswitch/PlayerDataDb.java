package com.mcm.core.serverswitch;

import com.mcm.core.Main;
import com.mcm.core.MySql;
import org.bukkit.Bukkit;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class PlayerDataDb {

    public static void create(String uuid) {
        Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, new Runnable() {
            @Override
            public void run() {
                try {
                    PreparedStatement statement = MySql.connection.prepareStatement("INSERT INTO Player_data(uuid, hash) VALUES (?, ?)");
                    statement.setString(1, uuid);
                    statement.setString(2, null);
                    statement.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static void updateData(String uuid, HashMap<String, Player_data> data) {
        if (getData(uuid) == null) create(uuid);
        Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, new Runnable() {
            @Override
            public void run() {
                try {
                    PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Player_data SET hash = ? WHERE uuid = ?");
                    statement.setString(2, uuid);
                    statement.setString(1, SerializerData.serializer(uuid, data));
                    statement.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static String getData(String uuid) {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("SELECT * FROM Player_data WHERE uuid = ?");
            statement.setString(1, uuid);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                return rs.getString("hash");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
