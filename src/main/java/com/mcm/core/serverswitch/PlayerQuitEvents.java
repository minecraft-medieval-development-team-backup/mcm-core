package com.mcm.core.serverswitch;

import com.mcm.core.Main;
import com.mcm.core.cache.isSwitch;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerQuitEvents implements Listener {

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        String uuid = player.getUniqueId().toString();

        Bukkit.getScheduler().runTaskLaterAsynchronously(Main.plugin, new Runnable() {
            @Override
            public void run() {
                if (isSwitch.get(uuid) == null) {
                    PlayerSwitch.sendData(player, "disconnect");
                } else isSwitch.get(uuid).delete();
            }
        }, 40L);
    }
}
