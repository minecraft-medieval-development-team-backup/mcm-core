package com.mcm.core.serverswitch;

import com.mcm.core.utils.ItemStackSerializer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class SerializerData {

    public static String serializer(String uuid, HashMap<String, Player_data> data) {
        String serialize = uuid + "/#/" + data.get(uuid).getConnect_to_server() + "/#/";
        for (ItemStack item : data.get(uuid).getItens()) {
            if (!serialize.substring(serialize.length() - 3).contains("/#/")) {
                serialize += "/:/" + ItemStackSerializer.serialize(item);
            } else serialize += ItemStackSerializer.serialize(item);
        }
        serialize += "/#/";

        for (ItemStack item : data.get(uuid).getArmors()) {
            if (!serialize.substring(serialize.length() - 3).contains("/#/")) {
                serialize += "/:/" + ItemStackSerializer.serialize(item);
            } else serialize += ItemStackSerializer.serialize(item);
        }
        serialize += "/#/";

        for (ItemStack item : data.get(uuid).getEnderChest()) {
            if (!serialize.substring(serialize.length() - 3).contains("/#/")) {
                serialize += "/:/" + ItemStackSerializer.serialize(item);
            } else serialize += ItemStackSerializer.serialize(item);
        }
        serialize += "/#/";

        serialize += ItemStackSerializer.serialize(data.get(uuid).getLeftHand()) + "/#/" + data.get(uuid).getFireTicks() + "/#/" + data.get(uuid).isAllowFlight() + "/#/" + data.get(uuid).getWalkSpeed() + "/#/" + data.get(uuid).getFlySpeed()
                + "/#/" + data.get(uuid).getLevel() + "/#/" + data.get(uuid).getExp() + "/#/" + data.get(uuid).getFoodLevel() + "/#/" + data.get(uuid).getHealthScale() + "/#/" + data.get(uuid).getHealth() + "/#/" + data.get(uuid).getGameMode().name() + "/#/";

        for (PotionEffect potion : data.get(uuid).getPotionEffects()) {
            serialize += potion.getType().getName() + ":::" + potion.getDuration() + ":::" + potion.getAmplifier() + "/";
        }
        System.out.println(serialize);
        return serialize;
    }

    public static HashMap<String, Player_data> deserialize(String serialized) {
        System.out.println(ChatColor.RED + serialized);
        String[] split = serialized.split("/#/");
        String uuid = split[0];
        String server_name = split[1];

        String[] itens = split[2].split("/:/");
        String[] armors = split[3].split("/:/");
        String[] enderChest = split[4].split("/:/");

        ArrayList<ItemStack> enderChestArray = null;
        ArrayList<ItemStack> armorsArray = null;
        ArrayList<ItemStack> itensArray = null;
        if (split[2] != null) {
            ArrayList<ItemStack> list = new ArrayList<>();
            for (String item : itens) {
                list.add(ItemStackSerializer.deserialize(item));
            }
            itensArray = list;
        }

        if (split[3] != null) {
            ArrayList<ItemStack> list = new ArrayList<>();
            for (String item : armors) {
                list.add(ItemStackSerializer.deserialize(item));
            }
            armorsArray = list;
        }

        if (split[3] != null) {
            ArrayList<ItemStack> list = new ArrayList<>();
            for (String item : enderChest) {
                list.add(ItemStackSerializer.deserialize(item));
            }
            enderChestArray = list;
        }

        Collection<PotionEffect> potionEffects = new ArrayList<>();
        if (split.length == 17) {
            if (split[16].contains("/")) {
                String[] potions = split[16].split("/");
                for (String potionS : potions) {
                    if (potionS != null) {
                        String[] pSplit = potionS.split(":::");
                        PotionEffect potion = new PotionEffect(PotionEffectType.getByName(pSplit[0]), Integer.valueOf(pSplit[1]), Integer.valueOf(pSplit[2]));
                        potionEffects.add(potion);
                    }
                }
            } else {
                String[] pSplit = split[16].split(":::");
                PotionEffect potion = new PotionEffect(PotionEffectType.getByName(pSplit[0]), Integer.valueOf(pSplit[1]), Integer.valueOf(pSplit[2]));
                potionEffects.add(potion);
            }
        }

        return new Player_data(uuid, server_name)
                .itens(itensArray.toArray(new ItemStack[0]))
                .armors(armorsArray.toArray(new ItemStack[0]))
                .enderChest(enderChestArray.toArray(new ItemStack[0]))
                .setLeftHand(ItemStackSerializer.deserialize(split[5]))
                .fireTicks(Integer.valueOf(split[6]))
                .allowFlight(Boolean.getBoolean(split[7]))
                .walkSpeed(Float.parseFloat(split[8]))
                .flySpeed(Float.parseFloat(split[9]))
                .level(Integer.valueOf(split[10]))
                .exp(Float.parseFloat(split[11]))
                .foodLevel(Integer.valueOf(split[12]))
                .healthScale(Double.parseDouble(split[13]))
                .health(Double.parseDouble(split[14]))
                .gameMode(GameMode.valueOf(split[15]))
                .potionEffects(potionEffects).insert();
    }
}
