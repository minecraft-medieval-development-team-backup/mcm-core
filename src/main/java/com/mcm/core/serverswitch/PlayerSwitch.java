package com.mcm.core.serverswitch;

import com.mcm.core.cache.isSwitch;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class PlayerSwitch {

    public static boolean sendData(Player player, String server_name) {
        String uuid = player.getUniqueId().toString();

        HashMap<String, Player_data> data = new Player_data(uuid, server_name)
                .itens(player.getInventory().getStorageContents())
                .armors(player.getInventory().getArmorContents())
                .enderChest(player.getEnderChest().getContents())
                .setLeftHand(player.getInventory().getItemInOffHand())
                .fireTicks(player.getFireTicks())
                .allowFlight(player.getAllowFlight())
                .walkSpeed(player.getWalkSpeed())
                .level(player.getLevel())
                .exp(player.getExp())
                .foodLevel(player.getFoodLevel())
                .healthScale(player.getHealthScale())
                .health(player.getHealth())
                .gameMode(player.getGameMode())
                .potionEffects(player.getActivePotionEffects()).insert();

        PlayerDataDb.updateData(uuid, data);
        if (!server_name.equals("disconnect")) {
            boolean sended = DataManager.sendData(uuid, data);
            new isSwitch(uuid).insert();
            return sended;
        }
        return false;
    }
}
