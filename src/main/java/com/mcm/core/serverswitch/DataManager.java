package com.mcm.core.serverswitch;

import com.mcm.core.Redis;
import java.util.HashMap;

public class DataManager {

    public static boolean sendData(String uuid, HashMap<String, Player_data> data) {
        if (Redis.localhost.get(uuid) == null) {
            Redis.localhost.set(uuid, SerializerData.serializer(uuid, data));
            return true;
        } else return false;
    }

    public static HashMap<String, Player_data> getData(String uuid) {
        HashMap<String, Player_data> data = SerializerData.deserialize(Redis.localhost.get(uuid));
        return data;
    }

    public static void deleteData(String uuid) {
        Redis.localhost.del(uuid);
    }
}