package com.mcm.core.serverswitch;

import org.bukkit.GameMode;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import java.util.Collection;
import java.util.HashMap;

public class Player_data {

    //left hand
    private String uuid;
    private String connect_to_server;
    private ItemStack[] itens;
    private ItemStack[] armors;
    private ItemStack[] enderChest;
    private ItemStack leftHand;
    private int fireTicks;
    private boolean allowFlight;
    private float walkSpeed;
    private float flySpeed;
    private int level;
    private float exp;
    private int foodLevel;
    private double healthScale;
    private double health;
    private GameMode gameMode;
    private Collection<PotionEffect> potionEffects;

    private static HashMap<String, Player_data> cache;

    public Player_data(String uuid, String connect_to_server) {
        this.cache = new HashMap<>();
        this.uuid = uuid;
        this.connect_to_server = connect_to_server;
    }

    public HashMap<String, Player_data> insert() {
        this.cache.put(this.uuid, this);
        return cache;
    }

    public String getConnect_to_server() {
        return this.connect_to_server;
    }

    public Collection<PotionEffect> getPotionEffects() {
        return this.potionEffects;
    }

    public Player_data potionEffects(Collection<PotionEffect> potionEffects) {
        this.potionEffects = potionEffects;
        return this;
    }

    public GameMode getGameMode() {
        return this.gameMode;
    }

    public Player_data gameMode(GameMode gameMode) {
        this.gameMode = gameMode;
        return this;
    }

    public double getHealth() {
        return this.health;
    }

    public Player_data health(double health) {
        this.health = health;
        return this;
    }

    public double getHealthScale() {
        return this.healthScale;
    }

    public Player_data healthScale(double healthScale) {
        this.healthScale = healthScale;
        return this;
    }

    public int getFoodLevel() {
        return this.foodLevel;
    }

    public Player_data foodLevel(int foodLevel) {
        this.foodLevel = foodLevel;
        return this;
    }

    public float getExp() {
        return this.exp;
    }

    public Player_data exp(float exp) {
        this.exp = exp;
        return this;
    }

    public int getLevel() {
        return this.level;
    }

    public Player_data level(int level) {
        this.level = level;
        return this;
    }

    public float getWalkSpeed() {
        return this.walkSpeed;
    }

    public Player_data walkSpeed(float walkSpeed) {
        this.walkSpeed = walkSpeed;
        return this;
    }

    public boolean isAllowFlight() {
        return this.allowFlight;
    }

    public Player_data allowFlight(boolean allowFlight) {
        this.allowFlight = allowFlight;
        return this;
    }

    public ItemStack[] getEnderChest() {
        return this.enderChest;
    }

    public Player_data enderChest(ItemStack[] enderChest) {
        this.enderChest = enderChest;
        return this;
    }

    public ItemStack[] getArmors() {
        return this.armors;
    }

    public Player_data armors(ItemStack[] armors) {
        this.armors = armors;
        return this;
    }

    public ItemStack[] getItens() {
        return this.itens;
    }

    public Player_data itens(ItemStack[] itens) {
        this.itens = itens;
        return this;
    }

    public int getFireTicks() {
        return fireTicks;
    }

    public Player_data fireTicks(int fireTicks) {
        this.fireTicks = fireTicks;
        return this;
    }

    public ItemStack getLeftHand() {
        return leftHand;
    }

    public Player_data setLeftHand(ItemStack leftHand) {
        this.leftHand = leftHand;
        return this;
    }

    public float getFlySpeed() {
        return flySpeed;
    }

    public Player_data flySpeed(float flySpeed) {
        this.flySpeed = flySpeed;
        return this;
    }
}
