package com.mcm.core.cache;

import java.util.HashMap;

public class StoreSignAdmin {

    private String location;
    private String item;
    private int quantity;
    private double purchase;
    private double sale;
    private static HashMap<String, StoreSignAdmin> cache = new HashMap<>();

    public StoreSignAdmin(String location, String item, int quantity, double purchase, double sale) {
        this.location = location;
        this.item = item;
        this.quantity = quantity;
        this.purchase = purchase;
        this.sale = sale;
    }

    public StoreSignAdmin insert() {
        this.cache.put(this.location, this);
        return this;
    }

    public static StoreSignAdmin get(String location) {
        return cache.get(location);
    }

    public void delete() {
        this.cache.remove(this.location);
    }

    public void updateItem(String item) {
        this.item = item;
    }

    public String getItem() {
        return item;
    }

    public void updateQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getQuantity() {
        return quantity;
    }

    public void updatePurchase(double purchase) {
        this.purchase = purchase;
    }

    public double getPurchase() {
        return purchase;
    }

    public void updateSale(double sale) {
        this.sale = sale;
    }

    public double getSale() {
        return sale;
    }
}
