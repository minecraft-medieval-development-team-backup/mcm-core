package com.mcm.core.cache;

import java.util.ArrayList;
import java.util.List;

public class LastHour {

    private static int players;
    private static List<String> list;

    public static int get() {
        return players;
    }

    public static void add(String uuid) {
        if (list == null) list = new ArrayList<>();
        if (!list.contains(uuid)) {
            players++;
            list.add(uuid);
        }
    }

    public static void reset() {
        players = 0;
        list.clear();
    }
}
