package com.mcm.core.cache;

import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class ItensMercado {

    public static ArrayList<String> idList;
    public static ArrayList<String> uuidList;

    private String id;
    private String uuid;
    private String name;
    private String category;
    private ItemStack item;
    private double price;
    private Date placed;
    private static HashMap<String, ItensMercado> cache = new HashMap<>();

    public ItensMercado (String id, String uuid, String name, String category, ItemStack item, double price, Date date) {
        this.id = id;
        this.uuid = uuid;
        this.name = name;
        this.category = category;
        this.item = item;
        this.price = price;
        this.placed = date;

        if (idList == null) idList = new ArrayList<>();
        idList.add(id);
        if (PlayerQtMarket.get(uuid) == null) new PlayerQtMarket(uuid).insert().add(); else PlayerQtMarket.get(uuid).add();
        if (uuidList == null) uuidList = new ArrayList<>();
        if (!uuidList.contains(uuid)) uuidList.add(uuid);
    }

    public ItensMercado insert() {
        this.cache.put(this.id, this);
        return this;
    }

    public static ItensMercado get(String id) {
        return cache.get(id);
    }

    public void removeItem() {
        PlayerQtMarket.get(this.uuid).remove();
        idList.remove(this.id);
        this.cache.remove(this.id);
    }

    public ItemStack getItem() {
        return item;
    }

    public void setItem(ItemStack item) {
        this.item = item;
    }

    public Date getPlaced() {
        return placed;
    }

    public void setPlaced(Date placed) {
        this.placed = placed;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
