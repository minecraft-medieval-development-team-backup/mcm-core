package com.mcm.core.cache;

import com.mcm.core.RabbitMq;
import com.mcm.core.database.RMqChatManager;
import java.util.ArrayList;
import java.util.HashMap;

public class TpaExpired {

    private static ArrayList<String> list = new ArrayList<String>();

    private static String player;
    private static String target;
    private static int times;
    private static HashMap<String, TpaExpired> cache = new HashMap<String, TpaExpired>();

    public TpaExpired (String player, String target) {
        this.player = player;
        this.target = target;
        this.times = 0;
    }

    public TpaExpired insert() {
        this.cache.put(player, this);
        return this;
    }

    public static TpaExpired get(String player) {
        return cache.get(player);
    }

    public void delete() {
        this.cache.remove(this.player);
    }

    public String getTarget() {
        return this.target;
    }

    public int getTimes() {
        return this.times;
    }

    public void setNewTimes() {
        this.times = 1;
    }

    //-

    public static void addNewUser(String player, String target) {
        list.add(player);
        new TpaExpired(player, target).insert();
    }

    public static void removeUser(String player) {
        list.remove(player);
        System.out.println(player);
        if (TpaExpired.get(player) != null) TpaExpired.get(player).delete();
    }

    public static void taskTpaExpired() {
        final Thread thread = new Thread(() -> {
            while (true) {
                try {
                    Thread.currentThread().sleep(60000);
                } catch (final Exception e) {
                    e.printStackTrace();
                }

                for (String player : list) {
                    if (get(player).getTimes() == 1) {
                        if (TpaInvitations.get(player) != null) {
                            String target = TpaExpired.get(player).getTarget();
                            TpaInvitations.get(player).removeList(target);

                            System.out.println("Target: " + target + " Player: " + player);
                            String server = PlayerLocation.get(target);
                            System.out.println(server);
                            if (server != null) {
                                RabbitMq.send(server, RMqChatManager.key + "/tpaexpired=" + target + "=" + "S8mdX6s#mGZxAyf///" + player + "///sEn&qGbm9vEW4fS");
                            }

                            list.remove(player);
                            get(player).delete();
                        } else {
                            list.remove(player);
                            get(player).delete();
                        }
                    } else if (get(player).getTimes() == 0) {
                        get(player).setNewTimes();
                    }
                }
            }
        });
        thread.setName("tpa.expired-thread");
        thread.start();
    }
}
