package com.mcm.core.cache;

import java.util.HashMap;

public class StoreSignPlayer {

    private String location;
    private String uuid;
    private int quantity;
    private double purchase;
    private double sale;
    private static HashMap<String, StoreSignPlayer> cache = new HashMap<>();

    public StoreSignPlayer(String location, String uuid, int quantity, double purchase, double sale) {
        this.location = location;
        this.uuid = uuid;
        this.quantity = quantity;
        this.purchase = purchase;
        this.sale = sale;
    }

    public StoreSignPlayer insert() {
        this.cache.put(this.location, this);
        return this;
    }

    public static StoreSignPlayer get(String location) {
        return cache.get(location);
    }

    public void delete() {
        this.cache.remove(this.location);
    }

    public String getUUID() {
        return this.uuid;
    }

    public void updateQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public void updatePurchase(double purchase) {
        this.purchase = purchase;
    }

    public double getPurchase() {
        return this.purchase;
    }

    public void updateSale(double sale) {
        this.sale = sale;
    }

    public double getSale() {
        return this.sale;
    }
}
