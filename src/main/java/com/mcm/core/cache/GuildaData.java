package com.mcm.core.cache;

import java.util.Date;
import java.util.HashMap;

public class GuildaData {

    private String name;
    private String membros;
    private int plot_level;
    private int coins_stock_level;
    private int elixir_stock_level;
    private int elixir_negro_stock_level;
    private int coins;
    private int elixir;
    private int elixir_negro;
    private long last_coins_collected;
    private long last_elixir_collected;
    private long last_elixir_negro_collected;
    private int generator_coins_level;
    private int generator_elixir_level;
    private int generator_elixir_negro_level;
    private int xp;
    private int level;
    private int wins;
    private int defeats;
    private Date last_presence;
    private Date last_attack_suffered;
    private Date last_attack_carried;
    private String alocated;
    private static HashMap<String, GuildaData> cache = new HashMap<>();

    public GuildaData(String name, String membros, int plot_level, int coins_stock_level, int elixir_stock_level, int elixir_negro_stock_level, int coins, int elixir, int elixir_negro, long last_coins_collected, long last_elixir_collected, long last_elixir_negro_collected, int generator_coins_level, int generator_elixir_level, int generator_elixir_negro_level, int xp, int level, int wins, int defeats, Date last_presence, Date last_attack_suffered, Date last_attack_carried, String alocated) {
        this.name = name;
        this.membros = membros;
        this.plot_level = plot_level;
        this.coins_stock_level = coins_stock_level;
        this.elixir_stock_level = elixir_stock_level;
        this.elixir_negro_stock_level = elixir_negro_stock_level;
        this.coins = coins;
        this.elixir = elixir;
        this.elixir_negro = elixir_negro;
        this.last_coins_collected = last_coins_collected;
        this.last_elixir_collected = last_elixir_collected;
        this.last_elixir_negro_collected = last_elixir_negro_collected;
        this.generator_coins_level = generator_coins_level;
        this.generator_elixir_level = generator_elixir_level;
        this.generator_elixir_negro_level = generator_elixir_negro_level;
        this.xp = xp;
        this.level = level;
        this.wins = wins;
        this.defeats = defeats;
        this.last_presence = last_presence;
        this.last_attack_suffered = last_attack_suffered;
        this.last_attack_carried = last_attack_carried;
        this.alocated = alocated;
    }

    public GuildaData insert() {
        this.cache.put(this.name, this);
        return this;
    }

    public static GuildaData get(String name) {
        return cache.get(name);
    }

    public void delete() {
        this.cache.remove(this.name);
    }

    public String getMembros() {
        return membros;
    }

    public void setMembros(String membros) {
        this.membros = membros;
    }

    public int getXp() {
        return xp;
    }

    public void setXp(int xp) {
        this.xp = xp;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getWins() {
        return wins;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public int getDefeats() {
        return defeats;
    }

    public void setDefeats(int defeats) {
        this.defeats = defeats;
    }

    public Date getLast_presence() {
        return last_presence;
    }

    public void setLast_presence(Date last_presence) {
        this.last_presence = last_presence;
    }

    public Date getLast_attack_suffered() {
        return last_attack_suffered;
    }

    public void setLast_attack_suffered(Date last_attack_suffered) {
        this.last_attack_suffered = last_attack_suffered;
    }

    public Date getLast_attack_carried() {
        return last_attack_carried;
    }

    public void setLast_attack_carried(Date last_attack_carried) {
        this.last_attack_carried = last_attack_carried;
    }

    public String getAlocated() {
        return alocated;
    }

    public void setAlocated(String alocated) {
        this.alocated = alocated;
    }

    public int getPlot_level() {
        return plot_level;
    }

    public void setPlot_level(int plot_level) {
        this.plot_level = plot_level;
    }

    public int getCoins_stock_level() {
        return coins_stock_level;
    }

    public void setCoins_stock_level(int coins_stock_level) {
        this.coins_stock_level = coins_stock_level;
    }

    public int getElixir_stock_level() {
        return elixir_stock_level;
    }

    public void setElixir_stock_level(int elixir_stock_level) {
        this.elixir_stock_level = elixir_stock_level;
    }

    public int getelixir_negro_stock_level() {
        return elixir_negro_stock_level;
    }

    public void setelixir_negro_stock_level(int elixir_negro_stock_level) {
        this.elixir_negro_stock_level = elixir_negro_stock_level;
    }

    public int getCoins() {
        return coins;
    }

    public void setCoins(int coins) {
        this.coins = coins;
    }

    public int getElixir() {
        return elixir;
    }

    public void setElixir(int elixir) {
        this.elixir = elixir;
    }

    public int getelixir_negro() {
        return elixir_negro;
    }

    public void setelixir_negro(int elixir_negro) {
        this.elixir_negro = elixir_negro;
    }

    public int getGenerator_coins_level() {
        return generator_coins_level;
    }

    public void setGenerator_coins_level(int generator_coins_level) {
        this.generator_coins_level = generator_coins_level;
    }

    public int getGenerator_elixir_level() {
        return generator_elixir_level;
    }

    public void setGenerator_elixir_level(int generator_elixir_level) {
        this.generator_elixir_level = generator_elixir_level;
    }

    public int getGenerator_elixir_negro_level() {
        return generator_elixir_negro_level;
    }

    public void setGenerator_elixir_negro_level(int generator_elixir_negro_level) {
        this.generator_elixir_negro_level = generator_elixir_negro_level;
    }

    public long getLast_coins_collected() {
        return last_coins_collected;
    }

    public void setLast_coins_collected(long last_coins_collected) {
        this.last_coins_collected = last_coins_collected;
    }

    public long getLast_elixir_collected() {
        return last_elixir_collected;
    }

    public void setLast_elixir_collected(long last_elixir_collected) {
        this.last_elixir_collected = last_elixir_collected;
    }

    public long getLast_elixir_negro_collected() {
        return last_elixir_negro_collected;
    }

    public void setLast_elixir_negro_collected(long last_elixir_negro_collected) {
        this.last_elixir_negro_collected = last_elixir_negro_collected;
    }
}
