package com.mcm.core.cache;

import java.util.HashMap;

public class PlayerQtMarket {

    private String uuid;
    private int quantity;
    private static HashMap<String, PlayerQtMarket> cache = new HashMap<>();

    public PlayerQtMarket (String uuid) {
        this.uuid = uuid;
        this.quantity = 0;
    }

    public PlayerQtMarket insert() {
        this.cache.put(this.uuid, this);
        return this;
    }

    public static PlayerQtMarket get(String uuid) {
        return cache.get(uuid);
    }

    public int getQuantity() {
        return quantity;
    }

    public void add() {
        this.quantity++;
    }

    public void remove() {
        this.quantity--;
    }
}
