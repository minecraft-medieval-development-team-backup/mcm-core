package com.mcm.core.cache;

import java.util.ArrayList;
import java.util.HashMap;

public class Mercado {

    private String category;
    private ArrayList<ItensMercado> itens;
    private static HashMap<String, Mercado> cache = new HashMap<>();

    public Mercado (String category) {
        this.category = category;
        this.itens = new ArrayList<>();
    }

    public Mercado insert() {
        this.cache.put(this.category, this);
        return this;
    }

    public static Mercado get(String category) {
        if (cache.get(category) == null) {
            Mercado a = new Mercado(category).insert();
            return a;
        } else return cache.get(category);
    }

    public ArrayList<ItensMercado> getItens() {
        return this.itens;
    }

    public void addItem(ItensMercado item) {
        this.itens.add(item);
    }

    public void removeItem(String id) {
        this.itens.remove(ItensMercado.get(id));
        ItensMercado.get(id).removeItem();
    }
}
