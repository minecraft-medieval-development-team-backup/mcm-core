package com.mcm.core.cache;

import com.mcm.core.enums.GuildsServers;
import com.mcm.core.enums.ServersList;

import java.util.HashMap;

public class OnlineCount {

    private String server;
    private int count;
    private static HashMap<String, OnlineCount> cache = new HashMap<>();

    public OnlineCount (String server, int count) {
        this.server = server;
        this.count = count;
    }

    public OnlineCount insert() {
        this.cache.put(this.server, this);
        return this;
    }

    public static OnlineCount get(String server) {
        return cache.get(server);
    }

    public int getCount() {
        return this.count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void delete() {
        this.cache.remove(this.server);
    }

    public static int getAllOnline() {
        int count = 0;
        for (ServersList server : ServersList.values()) {
            if (get(server.name().replaceAll("_", "-")) != null) {
                count += get(server.name().replaceAll("_", "-")).getCount();
            }
        }
        for (GuildsServers server : GuildsServers.values()) {
            if (get(server.name().replaceAll("_", "-")) != null) {
                count += get(server.name().replaceAll("_", "-")).getCount();
            }
        }
        return count;
    }
}
