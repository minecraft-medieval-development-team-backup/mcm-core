package com.mcm.core.cache;

import java.util.HashMap;

public class isSwitch {

    private String uuid;
    private static HashMap<String, isSwitch> cache = new HashMap<>();

    public isSwitch (String uuid) {
        this.uuid = uuid;
    }

    public isSwitch insert() {
        this.cache.put(this.uuid, this);
        return this;
    }

    public static isSwitch get(String uuid) {
        return cache.get(uuid);
    }

    public void delete() {
        this.cache.remove(this.uuid);
    }
}
