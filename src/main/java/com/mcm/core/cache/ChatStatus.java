package com.mcm.core.cache;

public class ChatStatus {

    public static boolean chatStatus;

    public static boolean getStatus() {
        return ChatStatus.chatStatus;
    }

    public static void setStatus(final boolean status) {
        ChatStatus.chatStatus = status;
    }

}
