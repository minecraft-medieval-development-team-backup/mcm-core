package com.mcm.core.cache;

import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class StoreGUI {

    private String category;
    private ItemStack categoryItem;
    private List<String> itens;
    private List<String> ids;
    private static HashMap<String, StoreGUI> cache = new HashMap<>();

    public static ArrayList<String> categories;

    public static void categoriesAdd(String category) {
        if (categories == null) categories = new ArrayList<>();
        if (!categories.contains(category)) categories.add(category);
    }

    public StoreGUI(String category, ItemStack categoryItem, List<String> itens) {
        this.category = category;
        this.categoryItem = categoryItem;
        this.itens = itens;
        this.ids = new ArrayList<>();

        StoreGUI.categoriesAdd(category);
    }

    public StoreGUI insert() {
        cache.put(this.category, this);
        return this;
    }

    public static StoreGUI get(String category) {
        return cache.get(category);
    }

    public void addID(String id) {
        this.ids.add(id);
    }

    public List<String> getIds() {
        return this.ids;
    }

    public void deleteCategory() {
        cache.remove(this.category);
    }

    public List<String> getItens() {
        return this.itens;
    }

    public void addItem(String item) {
        itens.add(item);
    }

    public void removeItem(String item) {
        itens.remove(item);
    }

    public void updateCategoryItem(ItemStack categoryItem) {
        this.categoryItem = categoryItem;
    }

    public ItemStack getCategoryItem() {
        return this.categoryItem;
    }
}
