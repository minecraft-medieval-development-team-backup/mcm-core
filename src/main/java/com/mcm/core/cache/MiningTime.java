package com.mcm.core.cache;

import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;

public class MiningTime {

    public static ArrayList<Player> players;

    private String uuid;
    private Player player;
    private int time;
    private static HashMap<String, MiningTime> cache = new HashMap<>();

    public MiningTime(String uuid, Player player, int time) {
        this.uuid = uuid;
        this.player = player;
        this.time = time;

        if (players == null) players = new ArrayList<>();
        players.add(player);
    }

    public MiningTime insert() {
        this.cache.put(this.uuid, this);
        return this;
    }

    public static MiningTime get(String uuid) {
        return cache.get(uuid);
    }

    public void delete() {
        players.remove(this.player);
        cache.remove(this.uuid);
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }
}
