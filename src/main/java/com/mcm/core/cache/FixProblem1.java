package com.mcm.core.cache;

import java.util.HashMap;

public class FixProblem1 {

    private String uuid_item_type;
    private static HashMap<String, FixProblem1> cache = new HashMap<>();

    public FixProblem1(String uuid_item_type) {
        this.uuid_item_type = uuid_item_type;
    }

    public FixProblem1 insert() {
        this.cache.put(this.uuid_item_type, this);
        return this;
    }

    public static FixProblem1 get(String uuid_and_item) {
        return cache.get(uuid_and_item);
    }
}
