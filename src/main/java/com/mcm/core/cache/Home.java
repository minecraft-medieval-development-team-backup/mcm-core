package com.mcm.core.cache;

import java.util.ArrayList;
import java.util.HashMap;

public class Home {

    private String uuid;
    private int size;
    private ArrayList<String> friends;
    private boolean move_perm;
    private static HashMap<String, Home> cache = new HashMap<>();

    public Home(String uuid, int size, ArrayList<String> friends, boolean move_perm) {
        this.uuid = uuid;
        this.size = size;
        this.friends = friends;
        this.move_perm = move_perm;
    }

    public Home insert() {
        this.cache.put(this.uuid, this);
        return this;
    }

    public static Home get(String uuid) {
        return cache.get(uuid);
    }

    public void delete() {
        cache.remove(this.uuid);
    }

    public ArrayList<String> getFriends() {
        return friends;
    }

    public void addFriend(String friend) {
        this.friends.add(friend);
    }

    public void removeFriend(String friend) {
        this.friends.remove(friend);
    }

    public boolean isMove_perm() {
        return move_perm;
    }

    public void setMove_perm(boolean move_perm) {
        this.move_perm = move_perm;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
