package com.mcm.core.cache;

import java.util.HashMap;

public class PlayerMine {

    private String uuid;
    private long time;
    private int xp;
    private int level;
    private static HashMap<String, PlayerMine> cache = new HashMap<>();

    public PlayerMine(String uuid, long time, int xp, int level) {
        this.uuid = uuid;
        this.time = time;
        this.xp = xp;
        this.level = level;
    }

    public PlayerMine insert() {
        this.cache.put(this.uuid, this);
        return this;
    }

    public static PlayerMine get(String uuid) {
        return cache.get(uuid);
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public int getXp() {
        return xp;
    }

    public void setXp(int xp) {
        this.xp = xp;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
