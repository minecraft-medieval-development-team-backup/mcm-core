package com.mcm.core.cache;

import java.util.ArrayList;
import java.util.HashMap;

public class InvitesGuilda {

    private String uuid;
    private ArrayList<String> list;
    private static HashMap<String, InvitesGuilda> cache = new HashMap<>();

    public InvitesGuilda(String uuid) {
        this.uuid = uuid;
        this.list = new ArrayList<>();
    }

    public InvitesGuilda insert() {
        this.cache.put(this.uuid, this);
        return this;
    }

    public static InvitesGuilda get(String uuid) {
        return cache.get(uuid);
    }

    public ArrayList<String> getList() {
        return list;
    }

    public void add(String name) {
        if (!this.list.contains(name)) this.list.add(name);
    }

    public void remove(String name) {
        this.list.remove(name);
    }
}
