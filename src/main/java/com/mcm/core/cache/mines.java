package com.mcm.core.cache;

import org.bukkit.Location;
import org.bukkit.Material;
import java.util.ArrayList;
import java.util.HashMap;

public class mines {

    public static ArrayList<Integer> mines;

    private int id;
    private String name;
    private String pos1;
    private String pos2;
    private ArrayList<Location> blocks;
    private ArrayList<Location> spawns;
    private ArrayList<Material> ores;
    private static HashMap<Integer, mines> cache = new HashMap<>();

    public mines(int id, String name, String pos1, String pos2, ArrayList<Location> blocks, ArrayList<Location> spawns, ArrayList<Material> ores) {
        this.id = id;
        this.name = name;
        this.pos1 = pos1;
        this.pos2 = pos2;
        this.blocks = blocks;
        this.spawns = spawns;
        this.ores = ores;

        if (mines == null) mines = new ArrayList<>();
        mines.add(id);
    }

    public mines insert() {
        this.cache.put(this.id, this);
        return this;
    }

    public static mines get(int id) {
        return cache.get(id);
    }

    public String getPos1() {
        return pos1;
    }

    public void setPos1(String pos1) {
        this.pos1 = pos1;
    }

    public String getPos2() {
        return pos2;
    }

    public void setPos2(String pos2) {
        this.pos2 = pos2;
    }

    public ArrayList<Material> getOres() {
        return ores;
    }

    public void setOres(ArrayList<Material> ores) {
        this.ores = ores;
    }

    public ArrayList<Location> getBlocks() {
        return blocks;
    }

    public void setBlocks(ArrayList<Location> blocks) {
        this.blocks = blocks;
    }

    public ArrayList<Location> getSpawns() {
        return spawns;
    }

    public void setSpawns(ArrayList<Location> spawns) {
        this.spawns = spawns;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
