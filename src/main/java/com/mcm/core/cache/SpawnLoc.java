package com.mcm.core.cache;

import java.util.HashMap;

public class SpawnLoc {

    private String servername;
    private String location;
    private static HashMap<String, SpawnLoc> cache = new HashMap<>();

    public SpawnLoc (String location) {
        this.servername = "server";
        this.location = location;
    }

    public SpawnLoc insert() {
        this.cache.put(this.servername, this);
        return this;
    }

    public static SpawnLoc getSpawnloc() {
        return cache.get("server");
    }

    public String getLocation() {
        return this.location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
