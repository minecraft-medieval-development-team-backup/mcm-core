package com.mcm.core.cache;

import java.util.ArrayList;
import java.util.HashMap;

public class TpaInvitations {

    private static String uuid;
    private static ArrayList<String> list;
    private static HashMap<String, TpaInvitations> cache = new HashMap<>();

    public TpaInvitations (String uuid) {
        this.uuid = uuid;
        this.list = new ArrayList<>();
    }

    public TpaInvitations insert() {
        this.cache.put(uuid, this);
        return this;
    }

    public static TpaInvitations get(String uuid) {
        return cache.get(uuid);
    }

    public ArrayList getList() {
        return this.list;
    }

    public void addList(String uuid) {
        this.list.add(uuid);
    }

    public void removeList(String uuid) {
        this.list.remove(uuid);
    }

    public void delete() {
        this.cache.remove(uuid);
    }
}
