package com.mcm.core.cache;

import java.util.HashMap;

public class Coins {

    private String uuid;
    private int coins;
    public static HashMap<String, Coins> cache = new HashMap<>();

    public Coins (String uuid, int coins) {
        this.uuid = uuid;
        this.coins = coins;
    }

    public Coins insert() {
        this.cache.put(uuid, this);
        return this;
    }

    public static Coins get(String uuid) {
        return cache.get(uuid);
    }

    public int getCoins() {
        return this.coins;
    }

    public void setCoins(int coins) {
        this.coins = coins;
    }
}
