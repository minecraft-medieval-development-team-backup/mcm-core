package com.mcm.core.cache;

import java.util.HashMap;

public class GuildaPlayer {

    private String uuid;
    private String guilda;
    private String role;
    private static HashMap<String, GuildaPlayer> cache = new HashMap<>();

    public GuildaPlayer(String uuid, String guilda, String role) {
        this.uuid = uuid;
        this.guilda = guilda;
        this.role = role;
    }

    public GuildaPlayer insert() {
        this.cache.put(this.uuid, this);
        return this;
    }

    public static GuildaPlayer get(String uuid) {
        return cache.get(uuid);
    }

    public void delete() {
        this.cache.remove(this.uuid);
    }

    public String getGuilda() {
        return guilda;
    }

    public void setGuilda(String guilda) {
        this.guilda = guilda;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
