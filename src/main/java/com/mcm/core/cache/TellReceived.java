package com.mcm.core.cache;

import java.util.HashMap;

public class TellReceived {

    private String name;
    private static HashMap<String, TellReceived> cache = new HashMap<>();

    public TellReceived(String name) {
        this.name = name;
    }

    public TellReceived insert() {
        this.cache.put(this.name, this);
        return this;
    }

    public static TellReceived get(String name) {
        return cache.get(name);
    }

    public void delete() {
        this.cache.remove(this.name);
    }
}
